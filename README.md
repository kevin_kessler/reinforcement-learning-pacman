This repository includes the following parts of our project "An Independent Learning PacMan Agent":

* The full documentation (LaTeX Sources)

* The short documentation (LaTeX Sources)

* The java sources

The project was part of our studies in Computer Science (M.Sc.).