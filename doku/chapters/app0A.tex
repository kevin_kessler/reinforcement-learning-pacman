\graphicspath{{images/appA/}}

\chapter{Implementierung der RL Applikation}
\label{app:app02}

\section{Verwendete Technologie}

Die im Rahmen dieser Projektarbeit erstellte Applikation basiert auf der Programmiersprache Java. 
Zusätzlich wird das GUI Framework JavaFX verwendet, welches von Oracle entwickelt und seit 2008 als De-facto-Nachfolger von Swing präsentiert wird (vgl. \cite{oracleJava}).
Es ist standardmäßig in allen aktuellen Versionen des JRE integriert\footnote{Stand der Informationen im Juli 2015} und kann für ältere Java Versionen separat heruntergeladen werden. 
Neben einer großen Menge an vordefinierten Komponenten wie Buttons und Formularfeldern, erlaubt das Framework zusätzlich das Einbinden von CSS. 
Darüber hinaus verfügt es zur einfacheren Gestaltung des User Interfaces über eine eigene Markupsprache namens FXML. 
Durch den Einsatz der JavaFX Dialoge wird zur Ausführung der Applikation mindestens eine JRE der Version 8.45 oder neuer benötigt (vgl. \cite{codeMakery}).

\section{Anwendungsarchitektur}

Das Softwareprojekt ist so strukturiert, dass es leicht wiederverwendet, erweitert und gewartet werden kann. Um dies zu erleichtern, werden im Folgenden die Hauptkomponenten sowie relevante Hilfskomponenten kurz erläutert.

\subsection{Hauptkomponenten}
\label{subs:main_components}
Gemäß des Model View Controller (MVC) Entwurfsmusters unterteilt sich die Anwendungsarchitektur, wie in Abbildung \ref{fig:appendix_a_architecture_simple} zu sehen, in drei Hauptkomponenten:

\begin{figure}[h!]
	\centering
	\includegraphics[width=1.0\textwidth]{architecture_simple}
	\caption{Architektur der Applikation mit allen existierenden Paketen}
	\label{fig:appendix_a_architecture_simple}
\end{figure}

\begin{itemize}
\item View: Die Darstellung des Frontends übernehmen die FXML Dateien des \textit{view} Packages. Dabei ist jeder Bereich der Applikation (z.B. Menü, Simulation, Task-Konfiguration etc.) in einer eigenen FXML Datei definiert. 
Außerdem existieren Custom Controls, die aus Standard-JavaFX Elementen zu komplexeren Einheiten zusammengesetzt wurden. 
Ein Beispiel hierfür sind die Felder der Labyrinthe, die in Kapitel \ref{chapter:rl_vis} in Abbildung \ref{fig:chap03_sim_szenario_1_sarsa} zu sehen sind. 
Sie umfassen neben einem Rectangle als äußeres UI Element auch vier Polygone und Labels für jede der Bewegungsrichtungen.
Die FXML Dateien besitzen keine direkte Verbindung zum Modell und werden von den Controller Klassen geladen, mit Daten gefüllt und angezeigt.
\item Controller: Die Steuerung der einzelnen Bereiche der Anwendung wird durch Controller Klassen des \textit{controller} Packages realisiert. 
Sie kontrollieren die Applikation indem sie die Models verwalten, FXML Dateien laden, dynamische Frontend-Elemente erstellen und auf die Eingaben des Benutzers reagieren. 
Sie können Aktionen wie beispielsweise das Trainieren des RL Agenten starten sowie abbrechen und werden vom Model über Änderungen informiert.
\item Model: Alle benötigten Daten werden in Klassen des \text{model} Packages vorgehalten. Sie sind unabhängig von der View und den Controllern. 
Allerdings erlauben sie es, dass sich Controllern bei ihnen als Beobachter registrieren und können somit Nachrichten an diese versenden, wenn relevante Daten geändert werden. 
Bewegt sich beispielsweise ein Objekt (PacMan oder Geist), dann informiert es alle beobachtenden Controller darüber, welche dann die View entsprechend anpassen können. 
Anders als beim klassischen MVC Ansatz, implementieren die Model Klassen auch Teile der Anwendungslogik wie beispielsweise die RL Algorithmen. 
Die Trennung der Reinforcement Learning Logik von den Controllern erlaubt es dem Agenten, Episoden auch im Hintergrund ohne visuelles Feedback zu absolvieren und somit entweder schnell zu trainieren oder in kurzer Zeit viele Spiel-Statistiken zu sammeln.
\end{itemize}

\subsection{Hilfskomponenten}
\label{subs:helper_components}
Neben den in Abschnitt \ref{subs:main_components} vorgestellten Hauptkomponenten existieren noch folgende, relevante Hilfskomponenten: 
\begin{itemize}
\item TaskPersistanceManager: Verwaltet vom Benutzer erzeugte und trainierte Taskobjekte.
Ein Taskobjekt umfasst einen RL-Agenten, seinen Lernfortschritt, seine Spielstatistiken und die vom Benutzer gewählten Umgebungsparameter. 
Der TaskPersistanceManager bietet Funktionen zum Laden, Speichern, Überschreiben und Löschen der Taskobjekte in Form von .task Dateien an.
Diese werden im \textit{tasks} Ordner der Anwendung abgelegt und können bei Bedarf auf andere Plattformen übertragen werden, um beispielsweise anderen Personen gesammelte Daten zur Verfügung zu stellen.
\item MazeLoader: Ist zuständig für die Verwaltung der Labyrinthe (engl. Mazes). 
Diese werden bei Bedarf aus den zugehörigen .pbm Dateien des \textit{mazes} Ordners ausgelesen und interpretiert.
PBM steht für Portable Bitmap Format und ist ein plattformunabhängiges Bildformat, welches die Pixel einer Grafik in Form einer Matrix beschreibt.
Diese Matrix wird beim Einlesen als zweidimensionales Array interpretiert\footnote{Das Einlesen der .pbm Dateien sowie das Umwandeln der Matrix zu einem zweidimensionalen Array basiert auf der TableLoader Klasse des in Abschnitt \ref{sec:pacman_environment} vorgestellten MASON Frameworks} und in ein Mazeobjekt konvertiert. 
Die einzelnen Elemente der Matrix bezeichnen jeweils einzelne Blöcke des Labyrinths. 
Die Art der Blöcke (Begehbar, Wand, Startposition, etc.) werden durch die Werte der Matrixelemente bestimmt. 
Die verschiedenen Werte und ihre Bedeutung können dem Enum \textit{BlockType} aus der Klasse \text{Maze} entnommen werden.
Demnach ist es auf einfache Weise möglich neue .pbm Dateien zu erstellen und der Anwendung dadurch weitere Labyrinthe für den Simulationsmodus und den RL-Modus hinzuzufügen.
\end{itemize}


\section{Reinforcement Learning Komponenten und Abhängigkeiten}
\label{app:app02_rl_components}

Die Applikation besteht aus zwei Teilen, die separat über das Menü gestartet werden können. Der erste Anwendungsteil umfasst die in Kapitel \ref{chapter:rl_vis} vorgestellte Simulationsumgebung zur Visualisierung der Algorithmen Value-Learning, Q-Learning und Sarsa. Der zweite demonstriert die Implementierung des Fallbeispiels PacMan, die auf Approximate Q-Learning aufbaut. 

Das Paket \textit{reinforcement\_learning} im Model kapselt für beide dieser Bereiche die benötigten Reinforcement Learning Algorithmen und Datenstrukturen. 
Abbildung \ref{fig:appendix_a_architecture_model} zeigt die wesentlichen Komponenten sowie interne und externen Abhängigkeiten. 

\begin{figure}[h!]
  \centering
    \includegraphics[width=1.0\textwidth]{architecture_model}
    \caption{UML Klassendiagramm des Model Packages mit den wichtigsten Reinforcement Learning Komponenten}
    \label{fig:appendix_a_architecture_model}
\end{figure}

Im Zentrum stehen die Klassen \textit{Agent} und \textit{Environment}, die dem Agenten und der Umgebung des Reinforcement Learning Szenarios entsprechen. 
Der Agent agiert anhand einer bestimmten Policy, die in der Anwendung durch ein Objekt der gleichnamigen Klasse repräsentiert wird.
Das \textit{Policy} Objekt besitzt sowohl einen Verweis auf ein Verfahren zur Aktionsauswahl (\textit{ActionSelection}) als auch auf einen Algorithmus zur Berechnung der Werte- bzw. Q-Werte (\textit{ValueFunction}). 
Damit dem Reinforcement Learning Prozess immer eine korrekte Policy zugrunde liegt, ermittelt die Klasse \textit{AgentFactory} anhand einer gegebenen Methode zur Aktionsauswahl und eines Reinforcement Learning Algorithmus, welche Art von Policy benötigt wird. 
Diese liefert die Factory zusammen mit einem geeignetem Agent Objekt zurück. 
Bei Value Learning erstellt sie beispielsweise einen Agenten mit einer \textit{ModelBasedPolicy} und einem zugehörigen \textit{TransitionModel} Objekt (vgl. auch Abschnitt \ref{sub:value_learning_visualization} \nameref{sub:value_learning_visualization}). Nachdem der Agent erstellt wurde, wird er in einem Objekt vom Typ Task abgelegt. 
Dieses steht in der gesamten Applikation global zur Verfügung und speichert neben dem Agenten auch die Benutzereinstellungen.

Die Visualisierung der Reinforcement Learning Algorithmen Value-Learning, Q-Learning und Sarsa-Learning, welche in Kapitel \ref{chapter:rl_vis} beschrieben ist, wird innerhalb der Anwendungsstruktur mit \textit{Simulation} bezeichnet.
Sowohl für die Simulation als auch für die Fallstudie PacMan existieren eigene Unterklassen von Environment und State innerhalb des \textit{environment} Packages. 
Beide Umgebungen erzeugen entsprechend des ausgewählten Labyrinths beim Starten der Simulation oder des PacMan Spiels die erforderlichen präterminalen Zustände. 
Außerdem halten sie Informationen zu den Belohnungen bzw. Strafen für das Erreichen des Endzustands vor. 
Allerdings ist eine Aufteilung zwischen diesen beiden Szenarien notwendig, da die Zustände für das PacMan Spiel auf Features basieren (vgl. auch Kapitel \ref{chapter:pacman}) und die Geister im Vergleich zur Simulation nicht statisch sind.





