\graphicspath{{images/chap01/}}

\chapter{Einleitung}
\label{chap:intro}

Die Idee des Reinforcement Learnings (RL, dt. Bestärkendes Lernen) stammt ursprünglich aus dem Bereich der  Interaktion zwischen Mensch und Tier (vgl. \cite{duswamy2013}, Abs. 1.1). 
So wurde beispielsweise im Artikel von \cite{ReynoldsHylandWickens01} untersucht, wie sich Tiere mit Hilfe von RL ausbilden lassen.
Verschiedene Bestandteile eines solchen Ausbildungsprozesses, wie beispielsweise ein lernendes Wesen (Agent) durch Strafen und Belohnungen zu einem bestimmten Verhalten zu animieren, sind auch im computergestützten Reinforcement Learning sinngemäß wiederzufinden. 
Unter den Disziplinen der Informatik wird RL als Teilgebiet des Machine Learnings eingeordnet und gehört somit zum Fachgebiet der künstlichen Intelligenz (KI).
Im Allgemeinen befassen sich Fragestellungen aus dem Bereich Reinforcement Learning damit, wie ein KI-Agent herausfinden kann, sich, in einer ihm unbekannten Umgebung, sinnvoll zu verhalten (vgl. \cite{suttonbarto98}, Abs. 18.1).
Aber anders als die meisten Formen des Machine Learnings die auf \textit{Supervised Learning} (SL) basieren, geht es beim Bestärkenden Lernen um das sogenannte \textit{Unsupervised Learning} (USL).
Im Gegensatz zu SL, wird dem lernenden Agenten bei USL nicht anhand von bereits vorliegenden Beispieldaten empfohlen, was er tun soll (vgl. \cite{suttonbarto98}, Abs. 1.1). 
Seine Entscheidungen und Aktionen basieren ausschließlich auf eigenen Erfahrungen.
Diese sammelt der Agenten in Form von numerischen Rückmeldungen der Umgebung. 
Je nachdem, wie sich seine Aktionen auf die Umwelt auswirken, fällt der Wert dieses Feedbacks entweder negativ (Strafe) oder positiv (Belohnung) aus.
Dabei werden die durchlebten Situationen durch Umgebungszustände repräsentiert und auf die korrespondierenden Aktionen abgebildet.
Der Agent merkt sich, welche Aktionen er in welchem Zustand getätigt hat und wie das Feedback daraufhin ausgefallen ist. 
Mit diesen Informationen kann er seine Entscheidungen anpassen, um während einer \textit{Episode} möglichst oft belohnt zu werden.
Eine Episode beschreibt eine Folge von Entscheidungen, beginnend beim Startzustand der Umgebung und endend mit dem Erreichen eines Endzustands (vgl. \cite{suttonbarto98}, Abs. 3.3).
Ziel des Agenten ist es, die in einer Episode erhaltene Gesamtbelohnung zu maximieren und sich dadurch, der Umgebung entsprechend, sinnvoll zu verhalten.
Folglich muss der Agent seine Umwelt besser kennenlernen, indem er in einem kontinuierlichen Trial-And-Error Verfahren mehrere Episoden durchläuft (\textit{USL-Prozess}). 

\section{Motivation}
\label{sec:motivation}
Obwohl die Geschichte von Reinforcement Learning bereits Jahrzehnte zurückreicht, ist das Thema höchst aktuell. 
Folgendes Beispiel aus dem Jahr 2013 zeigt, welche wirtschaftlich entscheidende Rolle RL zur Zeit in der Anwendungsentwicklung einnimmt. \\
DeepMind, ein junges, auf künstliche Intelligenz spezialisiertes Unternehmen aus London, entwickelt einen gleichnamigen RL-Algorithmus, der in der Lage ist verschiedene Spielszenarien zu analysieren und zu meistern.
Im Dezember 2013 wird eine Abhandlung veröffentlicht, welche am Beispiel des Atari-Spieleklassikers \textit{Breakout} das Potential von DeepMind demonstriert (vgl. \cite{deep2013}).
Dabei erzielt der Algorithmus Ergebnisse, die mit denen von professionellen menschlichen Benutzern vergleichbar sind. 
Bereits zwei Monate später wird DeepMind von Google übernommen (vgl. \cite{recode14}).
Seitdem wird die Weiterentwicklung mit Nachdruck vorangetrieben.


\section{Problemstellung}
Aufgrund der Aktualität sowie der wirtschaftlichen Bedeutung von KI und RL ist es nützlich sich bereits im Hochschulumfeld mit dieser Thematik zu befassen. 
Deshalb verfolgen die Autoren in erster Linie das Ziel, sich durch dieses Projekt in das Thema einzuarbeiten und dadurch ein besseres Verständnis von RL zu erlangen.  
Die dabei gewonnenen Eindrücke werden in dieser Arbeit dokumentiert, sodass auch anderen Studierenden ein vergleichsweise leichter Einstieg in das komplexe Thema ermöglicht wird.
Andere Hochschulen bieten ebenfalls Veranstaltungen und Projekte an, die es erlauben, sich praxisnah mit der programmatischen Lösung von RL Problemen auseinanderzusetzen (vgl. \cite{berkley_rlpacman}).
Die \textit{School of Computer Science and Engineering} Australiens hat beispielsweise eine Anwendung entwickelt (vgl. \cite{eku_catandmouseapplet}), welche die Vorgehensweise verschiedener RL-Algorithmen anhand des, oftmals in der Literatur verwendeten, Katz und Maus Szenarios demonstriert (vgl. \cite{mit_catandmouse}). 
Aufgrund des zu geringen Funktionsumfangs und wegen Nichteinhaltung gängiger Programmierrichtlinien eignet sich diese Applikation allerdings nur bedingt zum Einstieg in die Thematik und nur begrenzt zur Visualisierung der komplexen Zusammenhänge.
Deshalb entsteht im Rahmen dieser Arbeit eine eigene Demo-Anwendung.
Diese bietet einerseits Funktionen zur Visualisierung der Algorithmen an und dient gleichzeitig als Fallbeispiel, um untersuchen zu können, wie gut sich RL dazu eignet, einem Agenten das Spielen des Arcade-Klassikers \textit{PacMan} beizubringen.
Dementsprechend muss zuerst das Videospiel implementiert werden, bevor die Realisierung der RL Komponenten erfolgen kann. 
Da sich die vorliegende Arbeit auf die fachliche Darstellung von RL und der verwendeten Algorithmen beschränkt, werden die Details zur Umsetzung des PacMan-Spiels selbst nicht weiter ausgeführt.

\section{Aufbau der Arbeit}
Um dem Leser einen Überblick über den Aufbau der Arbeit zu verschaffen, wird im Folgenden der Inhalt jedes Kapitels kurz vorgestellt.

Während das aktuelle Kapitel die vorliegende Arbeit motiviert und ihren Zweck erläutert, befasst sich Kapitel \ref{chapter:theory} mit den theoretischen Grundlagen der Abhandlung.
Dazu wird zunächst die Basis von Reinforcement Learning, der Markov Decision Process, eingeführt und anhand von Beispielen erläutert. 
Darauf aufbauend wird die Theorie zum eigentlichen Reinforcement Learning behandelt.
Dabei werden verschiedene RL Lernmethoden vorgestellt, deren Unterschiede untereinander erläutert und Methoden zur Entscheidungsfindung eines RL-Agenten eingeführt.
Zuletzt schließt eine Übersicht über die eingeführte Theorie das Kapitel ab.

In Kapitel \ref{chapter:rl_vis} werden die Vorgehensweisen der vorgestellten Lernmethoden anhand von Beispielszenarien visualisiert und detailliert erläutert.
Dazu wird die Arbeitsweise der Algorithmen Schritt für Schritt erklärt und die Beispiele unter Verwendung der eingeführten Theorie vorgerechnet. 
Die Visualisierung der Szenarien basiert auf der für dieses Projekt entwickelten Anwendung, die dem Leser ermöglicht die Ausführungen praxisnah nachzuvollziehen. 

Kapitel \ref{chapter:pacman} stellt Informationen zum Fallbeispiel PacMan bereit.
Bevor auf die implementierte Spielumgebung eingegangen wird, folgen zunächst allgemeine Erläuterungen zu PacMan.
Anschließend werden die Überlegungen zur RL spezifischen Realisierung ausgeführt und begründet.
Im letzten Abschnitt des Kapitels werden einige von der Anwendung gesammelte Daten zu verschiedenen RL Spielszenarien untersucht und analysiert.
Die Auswertung gibt letztendlich Aufschluss über die Eignung der zuvor angestellten Überlegungen bezüglich des PacMan Szenarios.

Eine Zusammenfassung der Ergebnisse sowie ein Ausblick auf mögliche und sinnvolle Erweiterungen dieser Arbeit ist in Kapitel \ref{chapter:conclusion} zu finden.
Zusätzlich existieren zwei Anhänge.
In Anhang \ref{app:app02} wird grob die Umsetzung der entwickelten Anwendung beschrieben.
Dazu werden die verwendeten Technologie, die Anwendungsarchitektur und die spezifischen RL Komponenten dargelegt.
Abschließend bietet Anhang \ref{app:app01} Aufgaben für Studierende, die sich praktisch mit dem Thema auseinandersetzen oder ihren Wissensstand anhand verschiedener Verständnisfragen überprüfen wollen.
