\graphicspath{{images/chap04/}}

\chapter{Anwendungsbeispiel PacMan}
\label{chapter:pacman}
In den vorherigen Kapiteln wurde die Theorie zu Reinforcement Learning eingeführt und anhand des Simulations-Modus der Anwendung visualisiert.
In diesem Kapitel geht es um das Anwenden der Theorie auf den spezifischen Fall \textit{PacMan}.
Dazu wird zunächst allgemein die PacMan Umgebung eingeführt und anschließend die Realisierung des Szenarios mit Approximate Q-Learning erläutert. 
Dies beinhaltet  insbesondere die Erklärung der gewählten Feature-basierten Zustände sowie die Auswertung der Ergebnisse verschiedener Spiel-Szenarien, welche auf der vorliegenden Implementierung basieren.

\section{Umgebung}
\label{sec:pacman_environment}
Um die RL-Umgebung des Anwendungsbeispiels erläutern zu können, werden zunächst die Grundlagen des original Spiels kurz vorgestellt. 
In dem 1980 erschienen Arcade-Klassiker PacMan muss der gleichnamige Protagonist versuchen, sich in einem Labyrinth zurechtzufinden und dabei alle gelben Punkte (\textit{Dots}) zu fressen.
Dabei verliert er jedes Mal ein Leben, wenn er von einem der vier umherwandernden Geister berührt wird. 
Sinkt seine Lebensanzahl auf Null, verliert er das Spiel. 
Frisst er den letzten Dot eines Labyrinths, gewinnt er.
Die Geister folgen unterschiedlichen Verhaltensmustern, so dass sich beispielsweise Blinky (der rote Geist) immer auf kürzestem Weg zu PacMan hinbewegt, während sich Clyde (der orangene Geist) von PacMan entfernt, falls dieser ihm zu nahe kommt.\\
Diese und weitere Funktionen wurden für das Fallbeispiel implementiert. 
Die Umsetzung orientiert sich dabei teilweise an anderen Open Source Projekten. 
So basieren beispielsweise die Grafiken und KI der Geister auf der PacMan Implementierung des Visualisierungsframeworks MASON 
\footnote{MASON ist ein Framework für Java Simulationen, welches zahlreiche Visualisierungsanwendungen umfasst. Darunter befindet sich auch eine Implementierung von PacMan (vgl. \cite{mason})}.
Ein Kurs über künstliche Intelligenz, angeboten von der \textit{University of California, Berkeley}
\footnote{Der Kurs bietet eine große Menge an Informationen zu KI (vgl. \cite{berkley_aicourse}). Unter anderem werden RL Aufgaben am Beispiel von PacMan gestellt (vgl. \cite{berkley_rlpacman}).}
, lieferte Anregungen zur Implementierung der RL Aspekte.
Abbildung \ref{fig:runninggame} zeigt das Ergebnis der Umsetzung am Beispiel einer laufende Spielrunde.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{runninggame}
	\caption{Laufende Spielrunde mit Einstellungsmöglichkeiten}
	\label{fig:runninggame}
\end{figure}

Um verschiedene Szenarien untersuchen zu können, existieren unterschiedliche Funktionen zur Anpassung der Umgebung.
Es ist möglich zwischen verschiedenen Labyrinthen, Geisterkonstellationen und Dot-Belegungen zu wählen.
Die Werte der Belohnungen und Strafen, die der Agent während einer Episode sammelt, können ebenfalls angepasst werden.
Wie in Abbildung \ref{fig:config_of_possible_rewards} dargestellt, gibt es Belohnungen für einen Sieg (\textit{Win}) und für jeden gefressenen Punkt (Dot).
Strafen erhält der Agent, wenn er auf einen Geist stößt (\textit{Death}), sich auf ein leeres Feld bewegt (\textit{Step}) oder gegen die Wand läuft (\textit{Wall Hit}).
\imagewithwidth{config_of_possible_rewards}{Einstellungsmöglichkeiten für Belohnungen. Dabei entsprechen \textit{Penalties} (dt. Strafen) negativen Belohnungen.}{0.5}
Zu Beginn einer Episode befinden sich PacMan und die ausgewählten Geister jeweils auf den, durch das Labyrinth festgelegten, Startpositionen. 
Die Dots sind je nach gewählter Belegungsart im Labyrinth verteilt.
Eine Episode endet, wenn PacMan das Spiel auf zuvor beschriebene Weise gewinnt oder verliert.
Die Zustände der Umgebung können, im Gegensatz zu denen der Simulations-Umgebung aus Kapitel \ref{chapter:rl_vis}, nun nicht mehr allein durch die Position des Agenten beschrieben werden.
Dies liegt daran, dass sich die jetzige Umgebung, aufgrund sich bewegender Geister und verschwindender Dots, dynamisch verändert.
Abhängig von den gewählten Einstellungen, kann daher ein sehr großer Zustandsraum entstehen.
Wie in Abschnitt \ref{sub:approx_q_learning} beschrieben, können in diesem Fall nicht alle Zustände während des Lern-Prozesses besucht werden.
Hier ist das Zusammenfassen von vergleichbaren Situationen durch approximative Zustände notwendig.
Im Folgenden betrachten wir den Aufbau der, in der Anwendung verwendeten, approximativen Zustände.

\section{Features}
\label{sec:featuresstate}
Dieser Abschnitt liefert eine Beschreibung der Features von Approximate Q-Learning (siehe Abschnitt \ref{sub:approx_q_learning}) und deren beabsichtigten Zweck.
Ein Zustand, in der Implementierung \textit{FeaturesState} genannt, ist so realisiert, dass er bei der Erstellung alle benötigten Features berechnet.
Dazu wird auch die Bewegung von PacMan simuliert.
Die Geistbewegungen werden jedoch nicht berücksichtigt.
Befindet sich der Agent beispielsweise in \(s\) und hätte die Aktionen \(a_1,\dots,a_n\) zur Auswahl, dann wird für \(f(s,a_1)\) die Aktion \(a_1\) auf ihn angewandt, wodurch seine Bewegung simuliert und das Feature berechnet wird.
Dies wird für alle Aktionen und Features durchgeführt. \\

\noindent\textbf{WALKED\_INTO\_WALL }\(\;\;\;\;f_{\text{wall}}: S \times A \to \{0,1\}\) \\
\(f_{\text{wall}}(s,a)=1\), wenn PacMan gegen eine Wand läuft, sonst beträgt der Wert \(0\).
Dieses Feature soll verhindern, dass der Agent gegen die Wand läuft.
Da der Wertebereich keine negativen Zahlen enthält, muss das Gewicht durch das Lernen mit einer negativen Belohnung selbst negativ werden, um den Gesamtwert \(Q(s,a)\) (siehe Gl. \ref{eq:approx_q_learning}) zu mindern. \\

\noindent\textbf{GHOST\_THREAT} \(\;\;\;\;f_{\text{threat}}: S \times A \to  [0,4]\) \\
Die Bedrohung wird durch das Feature
\begin{equation}
	f_{\text{threat}}(s,a)= t_{g_1}(s,a) + t_{g_2}(s,a) + t_{g_3}(s,a)+ t_{g_4}(s,a)
\end{equation}
berechnet.
Die Funktion \(t_{g_i}:S \times A \to [0,1]\) repräsentiert die Bedrohung durch Geist \(i \in \{1,2,3,4\}\).
Wenn Geist \(i\) getroffen wird, gilt \(t_{g_i}(s,a)=1\).
In diesem Fall befinden sich Agent und Geist auf dem gleichen Feld. \(t_{g_i}(s,a)\) ist \(0\), wenn sich der \(i\)te Geist acht oder mehr Schritte von PacMan entfernt befindet\footnote{Die Distanz acht basiert auf \cite{ttb14pacman}.}.
Dazwischen wird linear interpoliert.
Durch dieses Feature soll der Agent die Nähe der Geister meiden. \\

\noindent\textbf{TRAPPED\_SITUATION} \(\;\;\;\;f_{\text{trapped}}: S \times A \to  \{0,1\}\) \\
\(f_{\text{trapped}}(s,a)\) ist \(1\), wenn  PacMan von Geistern umschlossen ist.
Damit ist gemeint, dass für den Agenten keine Möglichkeit besteht, den Geistern zu entkommen, es sei denn die Geister bewegen sich in dieser Situation suboptimal.
Der Agent soll nicht in solche Situationen gelangen, da er anschließend fast immer von einem Geist gefangen wird. \\
Es ist wichtig anzumerken, dass die Geistbewegungen nicht simuliert werden und somit nicht festzustellen ist, ob die Bewegung eines Geistes den PacMan umschließen würden.\\
Zur leichteren Realisierung wurde dieses Feature daher so implementiert, dass PacMan als nicht umschlossen gilt, wenn er sich mindestens acht Felder weit bewegen kann.
Es sind allerdings durchaus Labyrinthe denkbar, bei dem sich PacMan acht Felder bewegen könnte und dennoch umschlossen wäre. \\

\noindent\textbf{CLOSEST\_DISTANCE\_TO\_FOOD} \(\;\;\;\;f_{\text{food}}:S \times A \to  [0,1]\) \\
\(f_{\text{food}}(s,a)\) ist \(1\), wenn PacMan einen Dot isst und \(0\), wenn sich ein Dot
\begin{equation}
	d = \sqrt{w^2+h^2}
\end{equation}
Schritte oder weiter von dem Agenten entfernt befindet.
\(h\) und \(w\) stehen für die Höhe und die Breite des Labyrinths, was \(d\) zur Diagonale macht.\footnote{\(d\) dient lediglich als eine Länge, um \(f_{\text{food}}\) auf das Intervall \([0,1]\) normieren zu können.
Bei einem ersten Versuch wurde die Summe \(h+w\) anstelle von \(d\) verwendet, jedoch lieferte diese zu hohe Feature-Werte, so dass die meisten beobachteten Werte im Intervall \([0.8,1]\) lagen.
Die Diagonale hingegen liefert eine bessere Verteilung.}
Dieses Feature soll den Agenten zum Essen animieren.
Außerdem gilt \(f_{\text{food}}(s,a)=0 \), wenn \(f_{\text{wall}}(s,a)=1\). Dies soll verhindern, dass das Gewicht \(w_{\text{food}}\) durch das Laufen gegen eine Wand gemindert wird.\\

\noindent\textbf{GHOST\_IS\_A\_NEIGHBOR} \(\;\;\;\;f_{\text{neighbor}}: S \times A \to \{0,1\}\) \\
\(f_{\text{neighbor}}(s,a)\) ist \(1\), wenn sich ein Geist auf dem gleichen oder einem  benachbarten Feld (links, rechts, ober- oder unterhalb) von PacMan befindet.
Für alle anderen Fälle gilt \(f_{\text{neighbor}}(s,a)=0\).
Dieses Feature soll zusätzlich zu \(f_{\text{threat}}\) dazu beitragen, dass der Agent akuten Bedrohungen aus dem Weg geht.
Auch hier gilt \(f_{\text{food}}(s,a)=0 \), wenn \(f_{\text{neighbor}}(s,a)=1\), um eine Verminderung des Wertes zu verhindern.\\

\section{Approximate Q-Learning}
\label{sec:app_q_learning_applied}
Die fünf zuvor beschriebenen Features werden zur Umsetzung von Approximate Q-Learning verwendet. Die entsprechenden Gewichte
\begin{itemize}
	\item \(w_\text{wall}\),
	\item \(w_\text{threat}\),
	\item \(w_\text{neighbor}\),
	\item \(w_\text{trapped}\) und
	\item \(w_\text{food}\)
\end{itemize}
werden mit \(1.0\) initialisiert.
In jedem Schritt erhält der Agent den aktuellen Zustand \(s\) und die möglichen Aktionen \(A\).
In der Implementierung enthält \(s\) für alle Aktionen \(a \in A\) die vorberechneten numerischen Werte \( f_i(s,a)\) mit \(i \in \{\text{wall}, \text{threat}, \text{neighbor}, \text{trapped}, \text{food}\} \).
Der Agent bewertet die approximierten Q-Werte \(Q(s,a)\) nach Gl. \ref{eq:approx_q_learning} und sucht sich nach vorhandener Aktionsauswahl-Strategie (siehe Abschnitt \ref{subsec:action_selection}) eine Aktion aus.
PacMan wird gemäß der vom Agenten ausgesuchten Aktion \(a\) bewegt. 
Anschließend werden die Geister bewegt.
Am Ende eines Schrittes wird eine Belohnung an PacMan ausgeschüttet und die Gewichte \(w_i\) aktualisiert \footnote{Dies geschieht jedoch nur im Trainingsmodus. Wird mit einem bereits trainierten Agenten gespielt, verändern sich die gelernten Verhaltensweisen nicht mehr.}.
Hierzu wird Gl. \ref{eq:approx_q_learning_update} verwendet.

Folgend wird an Beispielen gezeigt, wie der Agent die Q-Werte berechnet und die Gewichte aktualisiert.
Die Gleichung zum Berechnen der Q-Werte lautet
\begin{equation}
\begin{split}
Q(s,a)  =& \;\;\;\;\, w_\text{wall} \; \cdot \; f_\text{wall}(s,a) \\
		 & + w_\text{threat} \; \cdot \; f_\text{threat}(s,a) \\
		 & + w_\text{trapped} \; \cdot \; f_\text{trapped}(s,a) \\
		 & + w_\text{food} \; \cdot \; f_\text{food}(s,a). \\
		 & + w_\text{neighbor} \; \cdot \; f_\text{neighbor}(s,a) \\
\end{split}
\end{equation}
Der Agent muss sich zwischen den Aktionen \(A = \{a_n, a_e, a_s, a_w\}\), die jeweils einer Bewegung in eine Himmelsrichtung entsprechen, entscheiden.
Es wird nun die Situation aus Abbildung \ref{fig:example_pac_man} mit den Einstellungen aus Abbildung \ref{fig:task_configuration} betrachtet.
\imagewithwidth{example_pac_man}{Es wird eine Situation mit vier Geistern und dem Labyrinth \textit{Standard 01} nach 1000 trainierten Episoden und einer Greedy-Policy betrachtet.}{0.5}
\imagewithwidth{task_configuration}{Die Einstellungen des Szenarios aus Abbilung \ref{fig:example_pac_man}. Die ausgegrauten Felder stellen nicht editierbare Attribute dar.}{1}
Im Folgenden werden Gewichte und Features als Vektoren
\begin{equation}
	\vec{w}=\begin{pmatrix} w_\text{wall} \\ w_\text{threat} \\ w_\text{trapped} \\ w_\text{food} \\ w_\text{neighbor}\end{pmatrix} 
	\qquad \text{und} \qquad
	\vec{f}_{s,a}=\begin{pmatrix} f_\text{wall}(s,a) \\ f_\text{threat}(s,a) \\ f_\text{trapped}(s,a) \\ f_\text{food}(s,a) \\ f_\text{neighbor}(s,a) \end{pmatrix}
\end{equation}
notiert.
Die gelernten Gewichte und die Features für den derzeitigen Zustand \(s\) kombiniert mit den Aktionen \(A\) ergeben sich zu
\begin{equation}
\vec{w} =\begin{pmatrix} 93.66 \\ -78.75 \\ -29.81 \\ 208.91 \\ 19.45 \end{pmatrix} \;
\vec{f}_{s,a_n}=\begin{pmatrix} 1 \\ 1.5 \\ 0 \\ 0 \\ 1 \end{pmatrix} \;
\vec{f}_{s,a_e}=\begin{pmatrix} 0 \\ 1.57 \\ 0 \\ 1 \\ 0 \end{pmatrix} \;
\vec{f}_{s,a_s}=\begin{pmatrix} 0 \\ 1.60 \\ 0 \\ 0 \\ 1 \end{pmatrix} \;
\vec{f}_{s,a_w}=\begin{pmatrix} 0 \\ 1.32 \\ 0 \\ 1 \\ 0 \end{pmatrix}
\end{equation}

Anders als beabsichtigt, ist das Gewicht für das Laufen gegen eine Wand nicht negativ, sondern positiv und sehr hoch.
Da \(f_{\text{food}}(s,a)=0 \), wenn \(f_{\text{wall}}(s,a)=1\), gilt und \(w_\text{food}\) sehr hoch ist, wird der Agent dennoch das Essen bevorzugen.
Deshalb und wegen zu großem Aufwand, wurden die genauen Gründe für den hohen Wert von $w_\text{wall}$ nicht weiter untersucht.
Zum Berechnen der Werte können wir nun das Skalarprodukt verwenden:


\begin{align*} 
Q(s,a_n) &= \vec{w} \cdot \vec{f}_{s,a_n} = \sum \limits_i w_i \cdot f_i \\ 
		 &= 93.66 \cdot 1 + (-78.75) \cdot 1.5 + (-29.81) \cdot 0 + 208.91 \cdot 0 + 19.45 \cdot 1 \\
		 &= -5.015 \\
Q(s,a_e) &= 85.2725 \\
Q(s,a_s) &= -106.55 \\
Q(s,a_w) &= 104.96 \\
\end{align*}

Da \(Q(s,a_w)\) den höchsten Wert besitzt, wird der Agent sich für die Aktion \(a_w\) entscheiden. Dies ist auch die einzig sinnvolle Richtung, in die sich PacMan bewegen sollte (siehe Abb. \ref{fig:example_pac_man}).
Die Umgebung belohnt daraufhin den Agenten für das Essen eines Dots mit der Belohnung \(r=40\). Anschließend befindet er sich in Zustand \(s'\).
Nun werden die Gewichte nach Gl. \ref{eq:approx_q_learning_update} aktualisiert.
Zunächst muss die Differenz der Zustände berechnet werden.
Die Berechnung des maximalen Q-Wertes von \(s'\) findet analog zu der obigen Rechnung statt und wird deswegen nicht im Detail durchgeführt.
Der Wert ist \(\text{max}_{a'}\;Q(s',a')= 97.87\).
Es liegt der Übergang \((s,a_w,r,s')\) vor und wir berechnen die Differenz \(\Delta\) mit
\begin{align}
\Delta &= [r+ \gamma \;\;  \underset{a'}{\text{max}} \;Q(s',a') ] - Q(s,a_w) \\
				 &= [40 + 0.9 \cdot 97.87] - 104.96 = 23.123.
\end{align}

Nun wird jedes einzelne Gewicht aktualisiert:

\begin{align}
w_i &\leftarrow w_i + \alpha \; \Delta f_i(s,a_w)\\
w_\text{wall} &\leftarrow w_\text{wall} + \alpha \; \Delta f_\text{wall}(s,a_w) \\
&\leftarrow 93.66 +  0.2 \cdot 23.123 \cdot 0 = 93.66 \\
w_\text{threat} &\leftarrow -78.75 + 0.2 \cdot 23.123 \cdot 1.32 = -72.65 \\
w_\text{trapped} &\leftarrow -29.81 + 0.2 \cdot 23.123 \cdot 0 = -29.81 \\
w_\text{food} &\leftarrow 208.91 + 0.2 \cdot 23.123 \cdot 1 =  213.53\\
w_\text{neighbor} &\leftarrow 19.45 + 0.2 \cdot 23.123 \cdot 0 = 19.45
\end{align}

Durch diesen Übergang wurden lediglich \(w_\text{threat}\) und \(w_\text{food}\) aktualisiert. Die restlichen Gewicht haben sich nicht verändert, da die zugehörigen Features \(0\) waren.
%Daten vom Debuggen:
%===================

% \gamma = 0.9
% \alpha = 0.2

%IDX_WALKED_INTO_WALL = 0
%IDX_GHOST_THREAT = 1
%IDX_TRAPPED_SITUATION = 2
%IDX_CLOSEST_DISTANCE_TO_FOOD = 3
%IDX_GHOST_IS_A_NEIGHBOR = 4
%weights auf 2 Nachkommastellen gerundet: [93.66, -78.75, -29.81, 208.91, 19.45]
%// WALKED_INTO_A_WALL ist nicht negativ!
%
%features auf 2 Nachkommastellen gerundet:
%0:GO_NORTH
%[1.0, 1.5, 0.0, 0.0, 1.0]
% 1.0 \\ 1.5 \\ 0.0 \\ 0.0 \\ 1.0

%1:GO_EAST
%[0.0, 1.573223304703363, 0.0, 1.0, 0.0]
% 0.0 \\ 1.57 \\ 0.0 \\ 1.0 \\ 0.0

%2:GO_SOUTH
%[0.0, 1.6047152924789525, 0.0, 0.0, 1.0]
% 0.0 \\ 1.60 \\ 0.0 \\ 0.0 \\ 1.0

%3:GO_WEST
%[0.0, 1.323223304703363, 0.0, 1.0, 0.0]
% 0.0 \\ 1.32 \\ 0.0 \\ 1.0 \\ 0.0
%
%Estimated Values:
% [GO_NORTH; Value: -5.01673434683461] , [GO_EAST; Value: 85.01877752739193] , [GO_SOUTH; Value: -106.92326920717638] , [GO_WEST; Value: 104.70683304999362]

% Action GO_WEST will be taken. A reward of 40 will be given to the agent for eating a dot,

% s'
%0:GO_NORTH
%[1.0, 1.5480283617670114, 0.0, 0.0, 1.0]
% 1 \\ 1.55 \\ 0 \\ 0 \\ 1

%1:GO_EAST
%[0.0, 1.75, 0.0, 0.0, 1.0]
% 0 \\ 1.75 \\ 0 \\ 0 \\ 1

%2:GO_SOUTH
%[1.0, 1.5480283617670114, 0.0, 0.0, 1.0]
%1 \\ 1.55 \\ 0 \\ 0 \\ 1

%3:GO_WEST
%[0.0, 1.4114745084375788, 0.0, 1.0, 0.0]
%[0 \\ 1.41 \\ 0 \\ 1 \\ 0


\section{Auswertung}
\label{sec:pacman_results}
Nachdem die Realisierung von Approximate Q-Learning am Fallbeispiel PacMan erläutert wurde, werden in diesem Abschnitt Ergebnisse diskutiert, Beobachtungen erwähnt und mögliche Verbesserungen vorgestellt.
Damit Daten beziehungsweise Ergebnisse analysiert werden können, verfügt die im Projekt entstandene Software über Funktionen, die es erlauben Spiel-Statistiken eines Agenten zu sammeln, zu visualisieren und miteinander zu vergleichen. 
Die Statistiken basieren jeweils auf sogenannten Trainingsleveln, wobei jedes Level den Wissenstand eines Agenten nach einer bestimmten Anzahl gelernter Episoden widerspiegelt. 
Das Trainingslevel des Agenten steigt nur solange er im Trainingsmodus lernt \footnote{Um den Agenten zu trainieren, kann die Task-Übersicht der Anwendung verwendet werden. Trainingsepisoden verlaufen dabei immer im Hintergrund. Eine Visualisierung des Spielfelds und der Spielzüge findet nicht statt.}. 
Ein so trainierter Agent kann zum Sammeln von Statistiken im Spielmodus\footnote{In diesem Modus können die Spielzüge im Labyrinth visuell nachverfolgt werden, wobei die zuvor gelernten Gewichte und Verhaltensmuster unverändert bleiben.} eingesetzt werden. 
Auf diese Weise kann das Spielverhalten des Agenten von verschiedenen Trainingsleveln leicht miteinander verglichen werden, was Rückschlüsse auf den Lernfortschritt über verschiedene Episoden hinweg ermöglicht. 
Zusätzlich kann der Agenten auf diese Weise zuerst in einem Szenario trainiert und sein Wissen danach in einem anderen Szenario getestet werden.

\imagewithwidth{cat_mouse_scenario}{Ein Szenario mit zwei Geistern und einem Dot.}{0.5}
\imagewithwidth{cat_mouse_configuration}{Die Einstellungen des Szenarios aus Abbildung \ref{fig:cat_mouse_scenario}.
Die ausgegrauten Felder stellen nicht editierbare Attribute dar.}{1}
\imagewithwidth{cat_mouse_perc_wins}{Die x-Achse zeigt das Trainingslevel des Agenten. Die y-Achse gibt den prozentualen Anteil von Siegen an, die außerhalb des Trainingsmodus errungen wurden.}{1}

Im ersten Beispiel betrachten wir ein Szenario, welches ähnlich zu dem Katz und Maus Problem aus \cite{eku_catandmouseapplet} aufgebaut ist.
Wie in Abbildung \ref{fig:cat_mouse_scenario} zu dargestellt, befinden sich zwei Geister in dem Labyrinth.
Die in diesem Beispiel verwendeten Einstellungen sind in Abbildung \ref{fig:cat_mouse_configuration} zu sehen.
Für dieses Szenario sind die entworfenen Features sehr gut.
Wie in Abbildung \ref{fig:cat_mouse_perc_wins} zu sehen, gewinnt PacMan bereits nach einer Trainingsepisode jede Spielrunde.
Beim Zuschauen stellt man fest, dass der Agent oft sehr intelligent agiert und die Geister austrickst, indem er beispielsweise gegen die Wand läuft, um danach besser von den Geistern weglaufen zu können.

Betrachtet man hingegen das standardmäßige PacMan-Szenario (Abb. \ref{fig:runninggame}) mit den Konfigurationen aus Abb. \ref{fig:task_configuration}, so fallen die Ergebnisse nicht so gut aus.
Die Statistiken in den Abbildungen \ref{fig:pacman_max_score}, \ref{fig:pacman_avg_score} und \ref{fig:pacman_perc_wins} belegen, dass die durchschnittlich gesammelten Punkte lediglich 1750 beträgt und die Maximalpunktzahl bei 2990 liegt.
Bereits nach 100 Episoden erreicht der Agent ein Leistungsniveau, das durch weitere Trainingsepisoden nicht mehr ansteigt.
Dies deutet daraufhin, dass sehr schnell gelernt wird.
Dadurch, dass der Agent so gut wie nie gewinnt, sieht man allerdings, dass die Features in dieser Weise nicht für solch ein Szenario geeignet sind.

\imagewithwidth{pacman_max_score}{Die maximal erreichte Punktzahl pro Trainingslevel.}{1}
\imagewithwidth{pacman_avg_score}{Das arithmetische Mittel aller erreichten Punktzahlen einer Episode pro Trainingslevel}{1}
\imagewithwidth{pacman_perc_wins}{Die x-Achse zeigt das Trainingslevel des Agenten. Die y-Achse gibt den prozentualen Anteil von Siegen an, die außerhalb des Trainingsmodus errungen wurden.}{1}


Insbesondere haben sich einige Features in den Beobachtungen anders entwickelt als erwartet:
\begin{itemize}
	\item Ein Beispiel dafür ist GHOST\_THREAT.
	Da PacMan beim Einsammeln von Dots oft einen Geist direkt hinter sich hat, entwickelt er den Eindruck, dass es positiv sei wenn ihn ein Geist beim Fressen verfolgt.
	Deshalb kommt es vor, dass er zu einem Geist hin läuft und erst dann mit dem Einsammeln der Dots weiter macht, wenn dieser ihn mit einem Feld Abstand verfolgt.
	\item Manchmal wurde GHOST\_THREAT sehr groß und beeinflusste die Entscheidungen zu stark.
	Dadurch zögerte PacMan in bestimmten Situationen einen Dot zu essen, obwohl ein menschlicher Spieler erkennen würde, dass das Risiko dafür nicht zu hoch wäre.
	\item In einem Beispiel wandte sich die Absicht von CLOSEST\_DISTANCE\_TO\_FOOD ins Gegenteil.
	Dies geschah in einem Szenario ohne Geister, einem großen Labyrinth und einer hohen Bestrafung für das Betreten leerer Felder.
	Hierbei wurde der Effekt beobachtet, dass \(w_{food}\) negativ wurde, obwohl das nicht beabsichtigt war.
	Es stellte sich heraus, dass der Agent beim Suchen nach Dots bei jeder Aktion eine negative Belohnung erhielt, da er auf seinem Weg immer wieder auf leere Felder stieß.
	\(w_{food}\) sank dadurch immer weiter ab, bis es schließlich negativ wurde.
	Auf diese Weise wurde dem Agenten vermittelt, dass es schlecht sei nach Dots zu suchen, was ihn dazu veranlasste sich nur noch im Kreis zu bewegen, wodurch das Szenario zu keinem Ende kam.
	\item Es scheint zudem so, dass das Erforschen (siehe Abschnitt \ref{subsec:exploration_exploitation}) kontraproduktiv bei Approximate Q-Learning ist.
	In vielen Szenarien verhielt sich der Agent sehr ungeschickt. Beispielsweise lief er oft gegen die Wand und vermied es Dots zu essen.
\end{itemize}

Aus zeitlichen Gründen konnte nicht weiter mit den Features experimentiert werden.
Es gibt mehrere Verbesserungsmöglichkeiten, die in zukünftigen Projekten realisiert werden könnten:
\begin{itemize}
	\item Eine wichtige Optimierung wäre die Einbeziehung der Geistbewegungen.
	Dadurch würde der Agent ein Schritt im Voraus denken.
	\item Damit PacMan nicht auf einen Geist wartet, der ihn mit einem Feld Abstand verfolgt, könnte die Belohnung für das Essen von Dots reduziert werden, sobald ein Geist ein Nachbar ist.
	\item Auf das Feature GHOST\_IS\_NEIGHBOR kann möglicherweise verzichtet werden, da GHOST\_THREAT ausreichen müsste.
	\item Es wäre denkbar, GHOST\_THREAT in Himmelsrichtungen aufzuteilen und somit bessere Ergebnisse zu erzielen.
	Ein erster Ansatz, bei dem die direkte Entfernung und die Richtung der Geister berücksichtigt wurden, wurde implementiert, jedoch nicht weiter verfolgt.
	Das Feature lieferte zu diesem Zeitpunkt keine besseren Ergebnisse. Womöglich verbessert es die Intelligenz in Kombination mit anderen Features.
	\item Die Distanz von acht Schritten für GHOST\_THREAT basiert auf \cite{ttb14pacman}.
	Eine Anpassung könnte zu besseren Ergebnissen führen.
	Beispielsweise wurden bei geringerer Distanz nur die näheren Geister berücksichtigt.
	Dadurch könnte der Agent angeregt werden Dots auch in solchen Situationen zu essen, in denen er zurzeit noch wegläuft.
\end{itemize}

Die Auswahl der Features ist der kritischste Schritt zum Lösen eines RL-Problems.
Man kann bei der Bestimmung der Features sehr viel Intelligenz vorprogrammieren.
Beispielsweise fließt bei Abschnitt 4 von \cite{ttb14pacman} die \textit{Richtung zum nächsten Ziel} in den Zustand mit ein.
Dies gibt dem Agent im Grunde bereits die nächste Richtung vor.
Was in diesem Fall gelernt werden muss, ist diese Richtungsangabe im Zustand richtig zu interpretieren.
Es stellt sich jedoch die Frage inwiefern dieses Vorgehen noch mit dem klassischen Reinforcement Learning Ansatz vereinbar ist. 
Eventuell wird hierdurch bereits zu viel Intelligenz vorprogrammiert.
Es besteht jedoch die Möglichkeit, dass komplexere Probleme nicht alleine durch RL gelöst werden können.
Bei diesen könnte eine Kombination aus Reinforcement Learning und vorgegebener Intelligenz zu besseren Resultaten führen.
