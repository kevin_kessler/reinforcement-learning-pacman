package controller;

import java.io.IOException;
import java.util.logging.Logger;

import model.environment.Settings;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * Abstact mother class of all controller classes in pacman project. Contains
 * methods to manage the scene and switching between views. Also stores a list
 * with all view files including name and path.
 * */
public abstract class Controller {

	protected final static Logger log = Logger.getLogger(Controller.class.getName());

	/**
	 * stage object is the main object for each javafx window and contains all
	 * other ui elements. To switch between views, it is necessary that the old
	 * ui elements are overwritten or removed from stage and the new ui elements
	 * are placed inside a container of the stage object.
	 */
	protected Stage stage;
	/**
	 * settings object containing information about the current selected task,
	 * the last visited view (for back button) and other application wide
	 * information
	 */
	protected Settings settings;

	/**
	 * Necessary for switching between the views.
	 * */
	public enum ViewFile {
		GAME_VIEW("/view/GameView.fxml"), 
		MAINMENU_VIEW("/view/MainMenuView.fxml"), 
		MAZESELECTION_VIEW("/view/MazeSelectionView.fxml"), 
		TASKCONFIGURATION_VIEW("/view/TaskConfigurationView.fxml"), 
		TASKOVERVIEW_VIEW("/view/TaskOverviewView.fxml"), 
		SIMULATION_VIEW("/view/SimulationView.fxml"), 
		STATISTICS_VIEW("/view/StatisticsView.fxml");

		/** relative file path of the view e.g. /view/MainMenuView.fxml */
		private final String relFilePath;

		/** creates a viewFile enum and takes a relFilePath */
		private ViewFile(final String relFilePath) {
			this.relFilePath = relFilePath;
		}

		/**
		 * returns the relative file path of the view e.g.
		 * /view/MainMenuView.fxml
		 */
		private String getRelFilePath() {
			return this.relFilePath;
		}
	}

	/**
	 * Method to save the stage for easy access by all controllers. Method is
	 * called in Main to set the stage initially. The stage object is needed to
	 * switch between views.
	 * 
	 * @param stage
	 *            the stage object of the application
	 * */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * Sets the Settings Object which contains information about the current
	 * selected task, the last visited view and other information that should be
	 * passed between the different views of the application.
	 * 
	 * @param settings
	 *            the settings object for the application
	 */
	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	/**
	 * gets called automatically when switching view, after initialisation, to
	 * provide safe access to stage object and settings
	 **/
	protected void afterInit() {
		// override if necessary
	}

	/**
	 * Loads the FXML File stored in a viewFile enum
	 * 
	 * @param viewFile
	 *            the viewFile enum which contains the relative file path to the
	 *            JavaFX FXML file
	 * 
	 * @return the FXMLLoader with the loaded javaFX FXML file
	 * */
	private FXMLLoader getFXMLLoader(ViewFile viewFile) {
		return new FXMLLoader(getClass().getResource(viewFile.getRelFilePath()));
	}

	/**
	 * Provides the Parent Element of the FXML File loaded by the FXMLLoader.
	 * The Parent element should be added to the stage object so the view can be
	 * visible to the user.
	 * 
	 * @param loader
	 *            the loader containing an loaded FXML file (view)
	 * 
	 * @return the root element of the FXML View as Parent object.
	 * */
	private Parent getRoot(FXMLLoader loader) {

		if (loader == null)
			return null;

		try {
			// try to fetch the root element from the fxml file. this will most
			// likely be
			// some kind of a layout e.g. gridlayout or anchorview.
			return (Parent) loader.load();
		} catch (IOException e) {
			log.severe("Could not load view:" + e.getMessage());
		}

		return null;
	}

	/**
	 * Method for displaying a new view to the user.
	 * 
	 * @param from
	 *            the view the user last visisted (or is currently visiting). Is
	 *            need for proper handling of the back button.
	 * @param to
	 *            the view which should be displayed to the user next.
	 * */
	protected void switchView(ViewFile from, ViewFile to) {
		log.info("Switching view to:" + to.getRelFilePath());

		// save this information so we can check where we want to go back if
		// user clicks "cancel"
		settings.setLastView(from);

		FXMLLoader loader = getFXMLLoader(to);
		Parent root = getRoot(loader);

		if (null == root)
			throw new RuntimeException("Could not load view " + to.getRelFilePath());

		// show view
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();

		// pass stage to the succeeding controller
		Controller ctrl = loader.getController();
		ctrl.setStage(stage);
		ctrl.setSettings(settings);
		// method we need in our controllers for all kind of actions with access
		// to settings
		ctrl.afterInit();
	}

	/**
	 * Shows a simple JavaFX Notification Dialog to the user which will wait for user input. 
	 * In other words it will block all other interaction with the ui except clicking on the
	 * dialog button.
	 * 
	 * Requires JDK 8u40 or newer.
	 * 
	 * @param title
	 *            the title of the window. text will be displayed in the header
	 *            bar of the dialog.
	 * @param header
	 *            header text of the dialog. will be displayed above the message
	 *            but below the title.
	 * @param msg
	 *            the main message displayed to the user below
	 * */
	protected void showMessage(String title, String header, String msg) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle((title == null) ? "Information" : title);
		if (header != null)
			alert.setHeaderText(header);
		if (msg != null)
			alert.setContentText(msg);
		// this will be a blocking dialog. User interaction is required.
		alert.showAndWait();
	}

}
