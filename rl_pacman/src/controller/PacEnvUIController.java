package controller;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import model.environment.EnvironmentChangeListener;
import model.environment.MoveAction;
import model.environment.Settings;
import model.environment.pacman.Blinky;
import model.environment.pacman.Clyde;
import model.environment.pacman.Dot;
import model.environment.pacman.Ghost;
import model.environment.pacman.Inky;
import model.environment.pacman.Maze;
import model.environment.pacman.MovableObject;
import model.environment.pacman.Pac;
import model.environment.pacman.PacEnvironment;
import model.environment.pacman.Pinky;
import model.reinforcement_learning.State;
import view.BlinkyUI;
import view.ClydeUI;
import view.DotUI;
import view.GhostUI;
import view.InkyUI;
import view.MovableUI;
import view.PacUI;
import view.PinkyUI;
import controller.timing.GameTimer;
import controller.timing.TimerFirable;

/**
 * Controller for the maze in the game view. Holds the PacEnvironment and all
 * Ghost, Pac UI Elemenents. Get's Notification by the environment for every
 * import event that occured (such as objectMoved or lastDotWasEaten) and
 * updates the ui elements accordingly.
 * */
public class PacEnvUIController extends Controller implements EnvironmentChangeListener, TimerFirable {

	private static final long TIME_PERIOD_FOR_GAME_TIMER = 250; // In ms

	/** holds the size of each block of the maze */
	private double blockSize = 16;

	/** necessary for determining the size of the pac */
	private double pacRadius = blockSize / 2.1f;

	/**
	 * timer which is running as long as the game is running. Is restarted every
	 * time after the end game animations (blinking ghost etc.) had stopped
	 */
	private GameTimer gameTimer;

	/** the gridPane to draw the game environment (maze, ghost, pac) to **/
	private GridPane envGridPane;
	private PacEnvironment env;

	/** the ui elements representing the ghosts and pac in the grid */
	private PacUI pacUI;
	private List<GhostUI> ghostUIs;
	private List<DotUI> dotUIs;

	/**
	 * used if the user is controlling the pac. will be set by gamecontroller if
	 * the user pressed a key.
	 */
	private MoveAction nextPacAction;

	/**
	 * is true if the timer was running but is stopped currently. will be used
	 * to reset the game after the blinking animation has finished
	 */
	private boolean timerWasRunning = false;

	/**
	 * will be true if the pac and ghosts should be reset to the start position
	 * e.g. if pac was caught by a ghost
	 */
	private boolean restartNecessary = false;

	/** will be true if an episode has ended (game won or lost) */
	private boolean resetNecessary = false;

	/**
	 * Creates a PacEnvUIController for controlling the maze in the game view
	 * 
	 * @param settings
	 *            the current settings object. is necessary to determine if this
	 *            is currently a manual game or not.
	 * @param environmentHolder
	 *            the ui element in game view where the maze including ghosts
	 *            and pac should be added
	 * */
	public PacEnvUIController(Settings settings, AnchorPane environmentHolder) {
		this.settings = settings;
		this.env = new PacEnvironment(settings.getTempTask());
		Maze maze = env.getMaze();

		// calc block size of the maze
		double blockwidth = environmentHolder.getWidth() / maze.getWidth();
		double blockheight = environmentHolder.getWidth() / maze.getWidth();
		blockSize = Math.max(blockwidth, blockheight);
		pacRadius = blockSize * 0.45;

		this.env.addEnvironmentChangedListener(this);
		// statistic data will get updated by events in the environment
		this.env.addEnvironmentChangedListener(settings.getTempTask().getStatisticsForCurrentTrainingLevel());

		this.gameTimer = new GameTimer(TIME_PERIOD_FOR_GAME_TIMER, this);

		this.ghostUIs = new ArrayList<GhostUI>();
		this.dotUIs = new ArrayList<DotUI>();
		this.envGridPane = initGridPane();
	}

	public String getPacLives() {
		return env.getPac().getLivesString();
	}

	public String getPacScore() {
		return env.getPac().getScoreString();
	}

	/**
	 * is called right after the initialisation of the view was finished. will
	 * set all ghosts, dots and the pac to their default position.
	 * */
	@Override
	protected void afterInit() {
		resetDots();
		resetGhostPositions();
		resetPacPos();
	}

	/**
	 * @return the GridPane including the maze, ghosts and pac
	 * */
	public GridPane getGridPane() {
		return this.envGridPane;
	}

	/**
	 * @return the environment with all information about the current running
	 *         episode, the pac model and ghost models. Offers the Possibility
	 *         to start an episode, executeAction and so on.
	 * */
	public PacEnvironment getEnvironment() {
		return this.env;
	}

	/**
	 * sets the next action for the pac
	 * 
	 * @param action
	 *            the action which should be the next action for the pac
	 */
	public void setNext(MoveAction action) {
		this.nextPacAction = action;
	}

	/**
	 * gets called if an object (pac, ghost) was moved. Moves the ui elements
	 * correspondingly.
	 * 
	 * @param movableObject
	 *            the object that was moved (will be an instance of ghost or pac
	 *            ui)
	 * @param hasNewPosition
	 *            is true if the moving of the object resulted in a new
	 *            position. false if the oject could not move to the desired
	 *            position (e.g. if there was a wall).
	 * @param isTeleport
	 *            is true if the action was a teleporting action (e.g. from left
	 *            to right in the maze, or back to start position)
	 * */
	@Override
	public void objectMoved(MovableObject movableObject, boolean hasNewPosition, boolean isTeleport) {

		final MovableUI movUI = getMoveableUIObjectFrom(movableObject);

		if (null == movUI)
			return;

		Platform.runLater(() -> {
			movUI.turnTo(movableObject.getLastAction());
			if (hasNewPosition) {
				// calc target screen coordinates for pac and start transition
				// towards it
				Point2D targetPosition = calcShapeScreenPosition(envGridPane, movUI);

				if (!isTeleport)
					movUI.moveTo(targetPosition.getX(), targetPosition.getY());
				else
					movUI.teleportTo(targetPosition.getX(), targetPosition.getY());
			}
		});
	}

	/**
	 * Helper method to determine which ui element belongs to the movableObject
	 * 
	 * @param movableObject
	 *            we want to know which ui elements belongs to this
	 *            movableObject
	 * @return the corresponding MovableUI
	 * */
	private MovableUI getMoveableUIObjectFrom(MovableObject movableObject) {
		if (movableObject instanceof Pac)
			return pacUI;
		else if (movableObject instanceof Ghost)
			return getGhostUIByGhost(ghostUIs, (Ghost) movableObject);
		else
			return null;
	}

	/**
	 * Is called by the environment if the last dot in the maze was eaten by
	 * pac. In this case the game is won and one episode has ended.
	 * 
	 * @param eatenDot
	 *            the glorious last dot that was eaten
	 * @param score
	 *            the reached score in this episode
	 * */
	@Override
	public void lastDotWasEaten(Dot eatenDot, Integer score) {
		DotUI dotUI = getDotUIByDot(dotUIs, eatenDot);
		removeDotUI(dotUI);
		gameWon();
	}

	/**
	 * Is called by environment if a dot was eaten by pac. the method will
	 * remove the dot from the grid which is representing the maze.
	 * */
	@Override
	public void dotWasEaten(final Dot eatenDot) {
		// Update UI here
		DotUI dotUI = getDotUIByDot(dotUIs, eatenDot);
		removeDotUI(dotUI);
	}

	/**
	 * removes the dotui from the list of existing dotui's and sets the dot to
	 * invisible.
	 * */
	public void removeDotUI(final DotUI eatenDot) {
		if (null != eatenDot) {
			dotUIs.remove(eatenDot);
			eatenDot.disappear();
		}
	}

	/**
	 * gets called by environment if the pac was caught by a ghost and therefore
	 * has lost one life. The method will execute a blink animation. after this
	 * animation all ghosts and the pac will be moved back to their start
	 * positions.
	 * 
	 * @param ghost
	 *            the ghost that ate the pac.
	 * */
	@Override
	public void pacWasCaughtBy(Ghost ghost) {
		// game will restart when blink ends
		timerWasRunning = gameTimer.isRunning();
		stopTimers();
		pacUI.playBlinkAnimation();
	}

	/**
	 * Performs one game cycle and resets or restarts the game if necessary.
	 */
	@Override
	public void fire() {

		if (resetNecessary) {
			reset();
			resetNecessary = false;
		} else if (restartNecessary) {
			restartRound();
			restartNecessary = false;
		}

		// if human player
		if (settings.isManualGame()) {
			if (null != nextPacAction && nextPacActionIsPossible()) {
				env.executeAction(nextPacAction);
			} else {
				env.executeAction(env.getPac().getLastAction());
			}
		} else {
			// else let the agent work...
			env.performOneTimeStep();
		}
	}

	/**
	 * Determines if the action stored in 'nextPacAction' is a possible action
	 * 
	 * @return true if the action would lead the pac to a walkable place. false
	 *         if the pac can't move in this direction (e.g. if there is a wall
	 *         blocking the desired position)
	 * */
	private boolean nextPacActionIsPossible() {
		return env.getPac().willNotRunIntoAWallBy(nextPacAction, env.getMaze());
	}

	/**
	 * stops the game timer.
	 * */
	public void stopTimers() {
		gameTimer.stop();
	}

	/**
	 * starts the game timer.
	 * */
	public void startTimers() {
		if (!gameTimer.isRunning())
			gameTimer.start();

	}

	/**
	 * initialises the grid pane which is representing the maze. adds the ghosts
	 * and the pac to the grid.
	 * */
	private GridPane initGridPane() {
		Maze maze = env.getMaze();
		GridPane gp = createGridPaneByMaze(maze, blockSize);

		// add pacman
		pacUI = new PacUI(env.getPac(), pacRadius, Color.YELLOW, TIME_PERIOD_FOR_GAME_TIMER);
		pacUI.playEatAnimation();

		// restarting game when blink ends
		pacUI.getBlinkAnimation().setOnFinished(new MovableBlinkingEventHandler(pacUI));

		// reset game when gameOverAnimation ends
		pacUI.getGameOverAnimation().setOnFinished(new PacGameOverEventHandler(pacUI));

		// putting pac to the upper left corner of the grid, since the
		// move-translation coordinate system
		// has its origin in this init position. thus we can calc move
		// coordinates by grid bounds.
		gp.add(pacUI, 0, 0);

		// add ghosts
		List<Ghost> ghosts = env.getGhosts();
		if (ghosts != null) {
			GhostUI ghostUI = null;
			for (Ghost g : ghosts) {
				if (g instanceof Inky) {
					ghostUI = new InkyUI((Inky) g, blockSize, TIME_PERIOD_FOR_GAME_TIMER);
				} else if (g instanceof Clyde) {
					ghostUI = new ClydeUI((Clyde) g, blockSize, TIME_PERIOD_FOR_GAME_TIMER);
				} else if (g instanceof Blinky) {
					ghostUI = new BlinkyUI((Blinky) g, blockSize, TIME_PERIOD_FOR_GAME_TIMER);
				} else {
					ghostUI = new PinkyUI((Pinky) g, blockSize, TIME_PERIOD_FOR_GAME_TIMER);
				}
				ghostUIs.add(ghostUI);

				// putting ghosts to the upper left corner of the grid, since
				// the move-translation coordinate system
				// has its origin in this init position. thus we can calc move
				// coordinates by grid bounds.
				gp.add(ghostUI, 0, 0);

				// ghosts blink when episode ended. reset game when blinking
				// finished
				ghostUI.getBlinkAnimation().setOnFinished(new MovableBlinkingEventHandler(ghostUI));
			}
		}

		return gp;
	}

	/**
	 * Handles the finished event of the game over animation of pac. will set
	 * resetNecessary to true and will start the gameTimer again if necessary
	 * */
	private class PacGameOverEventHandler implements EventHandler<ActionEvent> {

		private final PacUI pacUI;

		protected PacGameOverEventHandler(PacUI pacUI) {
			this.pacUI = pacUI;
		}

		@Override
		public void handle(ActionEvent actionEvent) {
			if (actionEvent != null && actionEvent.getSource() instanceof RotateTransition) {
				if (timerWasRunning)
					startTimers();
				resetNecessary = true;
				pacUI.stopGameOverAnimation();
			}
		}
	}

	/**
	 * Handles the finished event of the blink animation of the specific ghost
	 * or pac. will set resetNecessary to true and will start the gameTimer
	 * again if necessary
	 * */
	private class MovableBlinkingEventHandler implements EventHandler<ActionEvent> {

		private final MovableUI movUI;

		protected MovableBlinkingEventHandler(MovableUI movUI) {
			this.movUI = movUI;
		}

		@Override
		public void handle(ActionEvent actionEvent) {
			if (actionEvent != null && actionEvent.getSource() instanceof Timeline) {
				if (timerWasRunning)
					startTimers();
				resetNecessary = true;
				movUI.stopBlinkAnimation();
			}
		}
	}

	/**
	 * Resets Pac and Ghost positions. Used when Pac is caught and lives
	 * decremented, but not game over. * Should be invoked by setting
	 * restartNecessary = true. Direct calls may result in misbehaviour.
	 */
	private void restartRound() {
		// reset gui
		resetGhostPositions();
		resetPacPos();
		nextPacAction = null;
	}

	/**
	 * Resets Pac, Ghosts and Dots. Used when game over. Should be invoked by
	 * setting resetNecessary = true. Direct calls may result in misbehaviour.
	 */
	private void reset() {
		resetDots();
		resetGhostPositions();
		resetPacPos();
		nextPacAction = null;
	}

	/**
	 * teleports pac to pac start position
	 */
	private void resetPacPos() {
		Platform.runLater(() -> {
			Point2D initPacScreenPos = calcShapeScreenPosition(envGridPane, pacUI);
			pacUI.teleportTo(initPacScreenPos.getX(), initPacScreenPos.getY());
			pacUI.turnTo(MoveAction.GO_EAST);
			pacUI.toFront(); // put in front of dots
		});
	}

	/**
	 * teleports all ghosts to ghost area
	 */
	private void resetGhostPositions() {
		Platform.runLater(() -> {
			for (GhostUI ghostUI : ghostUIs) {
				Point2D initGhostScreenPos = calcShapeScreenPosition(envGridPane, ghostUI);
				ghostUI.teleportTo(initGhostScreenPos.getX(), initGhostScreenPos.getY());
				ghostUI.toFront(); // put in front of dots
			}
		});
	}

	/**
	 * places the ghostui in front of all other ui elements in the grid at this
	 * grid cell.
	 * */
	private void putGhostsToFront() {
		Platform.runLater(() -> {
			for (GhostUI ghostUI : ghostUIs) {
				ghostUI.toFront();
			}
		});
	}

	/**
	 * places the pacui in front of all other ui elements in the grid at this
	 * grid cell.
	 * */
	private void putPacToFront() {
		Platform.runLater(() -> {
			pacUI.toFront();
		});
	}

	/**
	 * clears the maze from remaining dots and repopulates it with new ones
	 */
	private void resetDots() {
		Platform.runLater(() -> {
			// remove remaining dots
			removeRemainingDotUIs();

			// add new dots
			for (Dot dot : env.getDots()) {
				DotUI dotUI = new DotUI(dot, 2, Color.GOLD, TIME_PERIOD_FOR_GAME_TIMER, envGridPane.getChildren());
				GridPane.setValignment(dotUI, VPos.CENTER);
				GridPane.setHalignment(dotUI, HPos.CENTER);
				envGridPane.add(dotUI, dot.getPosition().x, dot.getPosition().y);
				dotUIs.add(dotUI);
			}
		});
	}

	/**
	 * returns the node from the gridpane at the specific cell
	 * 
	 * @param gridpane
	 *            the source grid pane
	 * @param col
	 *            the desired column of the gridpane
	 * @param row
	 *            the desired row of the gridpane
	 * @return the node at the desired position with all its child elements
	 * */
	private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
		for (Node node : gridPane.getChildren()) {
			if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
				return node;
			}
		}
		return null;
	}

	/**
	 * Returns the corresponding dotui for a dot model
	 * */
	private DotUI getDotUIByDot(List<DotUI> dotUIs, Dot dot) {
		for (DotUI dotUI : dotUIs) {
			if (dot.equals(dotUI.getDot())) {
				return dotUI;
			}
		}
		return null;
	}

	/**
	 * returns the corresponding ghostui for a ghost model
	 * */
	private GhostUI getGhostUIByGhost(List<GhostUI> ghostUIs, Ghost ghost) {
		for (GhostUI ghostUI : ghostUIs) {
			if (ghost.equals(ghostUI.getGhost())) {
				return ghostUI;
			}
		}
		return null;
	}

	/**
	 * returns a 2D Point containing the position of the movableUI in the grid
	 * */
	private Point2D calcShapeScreenPosition(GridPane gp, MovableUI movableUI) {
		Point gridPos = movableUI.getGridPosition();
		Node n = getNodeFromGridPane(gp, gridPos.x, gridPos.y);
		Bounds b = n.getBoundsInParent();
		double x = b.getMinX() + b.getWidth() / 2 - movableUI.getWidth() / 2;
		double y = b.getMinY() + b.getHeight() / 2 - movableUI.getHeight() / 2;
		return new Point2D(x, y);
	}

	/**
	 * is called if the game is won to play a blink animation on ghosts and save
	 * the task progress.
	 * */
	private void gameWon() {
		if (!ghostUIs.isEmpty())
			blinkGhosts();
		else {
			resetNecessary = true;
		}
		if(!settings.isManualGame())
			settings.saveTask();
	}

	/**
	 * uesd to stop the timers and play the gameover animation on pac. saves the
	 * task.
	 * */
	private void gameLost() {
		timerWasRunning = gameTimer.isRunning();
		stopTimers();
		pacUI.playGameOverAnimation();
		if(!settings.isManualGame())
			settings.saveTask();
	}

	/**
	 * starts the blinking animation for all ghosts
	 * */
	private void blinkGhosts() {
		timerWasRunning = gameTimer.isRunning();
		stopTimers();
		for (GhostUI g : ghostUIs) {
			g.playBlinkAnimation();
		}
	}

	/**
	 * cancels all blinking and spinning animations
	 * */
	public void cancelAnimations() {

		if (pacUI.blinkAnimationIsRunning()) {
			pacUI.stopBlinkAnimation();
			restartNecessary = true;
		}

		if (pacUI.gameOverAnimationIsRunning()) {
			pacUI.stopGameOverAnimation();
			resetNecessary = true;
		}

		for (GhostUI g : ghostUIs) {
			if (g.blinkAnimationIsRunning()) {
				g.stopBlinkAnimation();
				resetNecessary = true;
			}
		}
	}

	/**
	 * removes all remaining dots from the grid. is necessary if the pac lost
	 * the game and we want to start a new episode.
	 * */
	private void removeRemainingDotUIs() {
		ObservableList<Node> gridChildren = envGridPane.getChildren();
		for (DotUI dotUI : dotUIs) {
			gridChildren.remove(dotUI);
		}
		dotUIs.clear();
	}

	/**
	 * creates a gridpane by the given maze which is stretched to the
	 * surrounding anchorpane. the gridpane will have the same row and column
	 * count as the maze. walls wil be blue, walkable areas will be black.
	 * */
	public static GridPane createGridPaneByMaze(Maze maze, double blockSize) {
		GridPane gp = new GridPane();

		// stretch the grid to fill its container
		AnchorPane.setTopAnchor(gp, 0.0);
		AnchorPane.setRightAnchor(gp, 0.0);
		AnchorPane.setBottomAnchor(gp, 0.0);
		AnchorPane.setLeftAnchor(gp, 0.0);

		// add maze ui
		for (int i = 0; i < maze.getWidth(); i++) {
			for (int j = 0; j < maze.getHeight(); j++) {
				Maze.BlockType bt = maze.getBlockTypeAt(i, j);
				Color c = (maze.isWalkable(bt) || maze.isGhostArea(bt)) ? Color.BLACK : Color.BLUE;
				Rectangle r = new Rectangle(blockSize, blockSize, c);
				gp.add(r, i, j);
			}
		}

		return gp;
	}

	/**
	 * is called by the environment if the value of the state has changed. Is
	 * not needed here because we are not displaying the state values in the
	 * maze grid.
	 */
	@Override
	public void stateValueChanged(State state) {
		// nothing to do here (only needed for simulationController)
	}

	/** is called by the environment if the episode was finished. */
	@Override
	public void episodeWasFinished(boolean canceled, int steps) {
		// nothing to do here
	}

	/**
	 * is called by the environment if the game is over. Will reset the
	 * 
	 * @param score
	 *            the total score of this episode as Integer
	 */
	@Override
	public void gameOver(Integer score) {
		gameLost();
	}

	/**
	 * resets all dots on the game field and puts the pac and ghosts in front if
	 * them (so dots will not cover the ghost and pac at their position).
	 * */
	@Override
	public void populatedDots() {
		resetDots();
		putGhostsToFront();
		putPacToFront();
	}

}