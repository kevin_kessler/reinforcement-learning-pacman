package controller.simulation;

import java.awt.Point;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.Task;
import model.environment.EnvironmentChangeListener;
import model.environment.MoveAction;
import model.environment.pacman.Dot;
import model.environment.pacman.Ghost;
import model.environment.pacman.Maze;
import model.environment.pacman.MovableObject;
import model.environment.pacman.Pac;
import model.environment.simulation.SimulationEnvironment;
import model.environment.simulation.SimulationState;
import model.reinforcement_learning.Action;
import model.reinforcement_learning.Agent;
import model.reinforcement_learning.AgentFactory;
import model.reinforcement_learning.State;
import model.reinforcement_learning.policy.ActionSelection;
import model.reinforcement_learning.policy.EpsGreedyActionSelection;
import model.reinforcement_learning.policy.GreedyActionSelection;
import model.reinforcement_learning.training.TrainingListener;
import model.reinforcement_learning.value_function.QLearning;
import model.reinforcement_learning.value_function.Sarsa;
import model.reinforcement_learning.value_function.Value;
import model.reinforcement_learning.value_function.ValueFunction;
import model.reinforcement_learning.value_function.ValueLearning;
import utils.MazeLoader;
import utils.TextfieldConverterHelper;
import utils.TrainingHelper;
import view.DotUI;
import view.PacUI;
import view.QValueCellControl;
import view.StaticGhostUI;
import view.ValueCellControl;
import controller.Controller;

/**
 * Controller class for the simulation of value iteration and q-learning. The
 * View holds a grid with all possible states of the environment. For Q-Learning
 * each grid cell is devided in 4 regions (one for every possible action of the
 * pac) see also:
 * https://www.youtube.com/watch?v=IXuHxkpO5E8&feature=youtu.be&t=1h32s
 * */
public class SimulationController extends Controller implements Initializable, EnvironmentChangeListener, TrainingListener {

	protected final static Logger log = Logger.getLogger(SimulationController.class.getName());
	private final static String SIMULATION_MAZE_NAME = "Simulation";

	@FXML
	private AnchorPane gridHolder;

	@FXML
	public ComboBox<String> combbTrainingMethod, combbSelectionPolicy, combbMaze;

	@FXML
	public TextField tfGamma, tfAlpha, tfTrainingNumber, tfEpsilon;

	@FXML
	public Button btnDoTraining, btnDoEpisode, btnStopEpisode, btnDoStep, btnEditSave;

	@FXML
	public Label lbActionSelection;
	public static final String EPSILON_LABEL_TEXT = "Epsilon (Action Selection)";
	public static final String TEMPERATUR_LABEL_TEXT = "Temperature (Action Selection)";

	private String simulationMaze = SIMULATION_MAZE_NAME;
	private Maze maze;

	private SimulationEnvironment env;
	private GridPane grid;
	private double blockSize;
	private double pacRadius;
	private PacUI pacUI;
	private List<DotUI> dotUIs = new ArrayList<>();
	private List<StaticGhostUI> ghostUIs = new ArrayList<>();

	private HashMap<Long, QValueCellControl> allQValueCellControls;
	private HashMap<Long, ValueCellControl> valueCellControls;

	private Task currentTask;

	/**
	 * defines the state of the simulation and influences which buttons and
	 * fields are enabled / disabled. is true if we clicked the "Edit" button
	 * below the settings
	 */
	private boolean isEditing = false;

	/**
	 * true if we are currently training the agent by using the
	 * "do Episodes (invisible)" option
	 */
	private boolean isPlayingInBackground = false;

	/** true if we are currently playing a single episode by "doEpisode" */
	private boolean isAutoplay = false;

	private static final String EDIT_STRING = "Edit";
	private static final String SAVE_STRING = "Adopt Values";

	@FXML
	private Label lblCurrentEpisodes;

	/**
	 * is called while initialising the view. it would be possible to add
	 * dynamically created ui elements here before displaying anything to the
	 * user. in case of this view we need our settings object, but the settings
	 * object is not accessible in initialize because creating the view is not
	 * completed. therefore we use the afterInit method, which is called right
	 * after initialisation of the view has finished.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * is called right after initialisation of the view has finished. the
	 * methods creates a default task at the beginning with a default agent and
	 * maze. Afterwards all ui elements are initialised and shown to the user.
	 **/
	@Override
	public void afterInit() {

		// create a default task for simulation
		currentTask = new Task();
		currentTask.setAgent(AgentFactory.getDefaultAgent());
		currentTask.setMazeName(simulationMaze);

		// load maze
		this.maze = MazeLoader.getInstance().getSimulationMaze(currentTask.getMazeName());
		env = new SimulationEnvironment(maze);
		env.setCurrentTask(currentTask);
		env.addEnvironmentChangedListener(this);
		env.enableLearning(true);

		settings.setRlTask(currentTask);

		loadGrid();
		initComboBoxes();
		fillUIElements();
		enableDisableButtonsAndFields();

		setupKeyHandler();
	}

	/**
	 * Fills and enabled or disables the textfields for alpha, gamma and epsilon
	 * depending on the current tasks value function and action selection
	 * method.
	 * */
	private void fillUIElements() {

		ValueFunction valFunc = currentTask.getAgent().getPolicy().getValueFunction();

		// we most likely will have to set other values for other training
		// methods;
		if (valFunc instanceof ValueLearning) {
			ValueLearning valFuncIt = (ValueLearning) valFunc;
			tfGamma.setText(Double.toString(valFuncIt.getDiscountRate()));
			tfAlpha.setText(Double.toString(valFuncIt.getLearningRate()));
		} else if (valFunc instanceof QLearning) {
			QLearning valFuncQIt = (QLearning) valFunc;
			tfGamma.setText(Double.toString(valFuncQIt.getDiscountRate()));
			tfAlpha.setText(Double.toString(valFuncQIt.getLearningRate()));
		} else {
			tfGamma.setDisable(true);
			tfAlpha.setDisable(true);
		}

		ActionSelection actSelMethod = currentTask.getAgent().getPolicy().getActionSelectionMethod();
		if (actSelMethod instanceof GreedyActionSelection) {
			tfEpsilon.setText(Double.toString(0.3));
			tfEpsilon.setDisable(true);
		} else if (actSelMethod instanceof EpsGreedyActionSelection) {
			EpsGreedyActionSelection epsGreedy = (EpsGreedyActionSelection) actSelMethod;
			tfEpsilon.setText(Double.toString(epsGreedy.getEpsilon()));
			tfEpsilon.setDisable(false);
		}

	}

	/**
	 * Fills the comboboxes for maze, action selection method and training
	 * method with initial values, depending on which values are available in
	 * the corresponding classes. The available value functions for example are
	 * read from the ValueFunction mother class.
	 * */
	private void initComboBoxes() {

		// VALUE FUNCTION COMBOBOX
		ObservableList<String> valueFunctionNames = FXCollections.observableArrayList(ValueFunction.valueFunctions);
		combbTrainingMethod.setItems(valueFunctionNames);

		// MAZE COMBOBOX
		ObservableList<String> mazeNames = FXCollections.observableArrayList(MazeLoader.getInstance().getSimulationMazeNames());
		combbMaze.setItems(mazeNames);
		combbMaze.getSelectionModel().select(currentTask.getMazeName());

		// ACTION SELECTION COMBOBOX
		ObservableList<String> actionSelectionNames = FXCollections.observableArrayList(ActionSelection.ACTION_SELECTION_METHODS);
		combbSelectionPolicy.setItems(actionSelectionNames);
		combbSelectionPolicy.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				switch (newValue) {
				case ActionSelection.EPSGREEDY:
					tfEpsilon.setDisable(false);
					lbActionSelection.setText(EPSILON_LABEL_TEXT);
					break;

				case ActionSelection.GREEDY:
					tfEpsilon.setDisable(true);
					tfEpsilon.setText("1.0");
					lbActionSelection.setText(EPSILON_LABEL_TEXT);
					break;

				case ActionSelection.SOFTMAX:
					tfEpsilon.setDisable(false);
					lbActionSelection.setText(TEMPERATUR_LABEL_TEXT);
					break;

				default:
					throw new RuntimeException("Action Selection Policy is not supported.");
				}
			}
		});

		Agent agent = currentTask.getAgent();
		if (agent != null && agent.getPolicy() != null) {
			combbSelectionPolicy.getSelectionModel().select(agent.getPolicy().getActionSelectionMethod().getName());
			if (agent.getPolicy().getValueFunction() != null) {
				combbTrainingMethod.getSelectionModel().select(agent.getPolicy().getValueFunction().getName());
			}
		}

	}

	/**
	 * Changes maze in task and environment. Repaint will be done by save()
	 * method in loadGrid()
	 * 
	 * @param newMazeName
	 *            the name of the new maze as string. will be loaded by
	 *            MazeLoader.
	 */
	private void changeMaze(String newMazeName) {
		currentTask.setMazeName(newMazeName);
		maze = MazeLoader.getInstance().getSimulationMaze(currentTask.getMazeName());
		env = new SimulationEnvironment(maze);
		env.addEnvironmentChangedListener(this);
		env.setCurrentTask(currentTask);
		env.enableLearning(true);
	}

	/**
	 * loads the grid for the simulation. the grid rows and columns equals the
	 * rows and columns of the current maze. the size of the grid cells depends
	 * on the size of the grid holder anchorPane.
	 * */
	private void loadGrid() {

		if (grid != null && gridHolder.getChildren().contains(grid)) {
			gridHolder.getChildren().remove(grid);
		}

		double blockwidth = gridHolder.getMaxWidth() / maze.getWidth();
		double blockheight = gridHolder.getMaxHeight() / maze.getHeight();
		blockSize = Math.min(blockwidth, blockheight);
		pacRadius = blockSize * 0.15;

		grid = createGridPane();

		gridHolder.getChildren().add(grid);

		resetDots();
		resetGhosts();
		resetPac();
	}

	/**
	 * creates a grid pane with labels and rectangles in each cell. the labels
	 * hold the values for each state and the rectangles are used for changing
	 * the background color of each cell corresponding to the value of the
	 * state.
	 * */
	private GridPane createGridPane() {
		GridPane gp = new GridPane();

		gp.setMaxHeight(blockSize * maze.getHeight());
		gp.setMaxWidth(blockSize * maze.getWidth());

		// stretch the grid to fill its container
		AnchorPane.setTopAnchor(gp, 0.0);
		AnchorPane.setRightAnchor(gp, 0.0);
		AnchorPane.setBottomAnchor(gp, 0.0);
		AnchorPane.setLeftAnchor(gp, 0.0);

		// Every state in the beginning has value 0.0
		double startValue = 0.0;

		// it depends on the value function type how much labels we need
		ValueFunction vf = currentTask.getAgent().getPolicy().getValueFunction();

		if (valueCellControls == null)
			valueCellControls = new HashMap<Long, ValueCellControl>();

		for (int x = 0; x < maze.getWidth(); x++)
			for (int y = 0; y < maze.getHeight(); y++) {

				Maze.BlockType bt = maze.getBlockTypeAt(x, y);

				if (maze.isWalkable(bt)) {

					if (vf instanceof QLearning && !env.isPreTerminalState(new SimulationState(maze.getPointAt(x, y)))) {

						// If we use a training algorithm with q-values we need
						// the qvaluecell xml

						if (allQValueCellControls == null)
							allQValueCellControls = new HashMap<Long, QValueCellControl>();

						QValueCellControl qValCell = new QValueCellControl(startValue, startValue, startValue, startValue);

						allQValueCellControls.put(getIdentifier(x, y), qValCell);
						gp.add(qValCell, x, y);
						GridPane.setHalignment(qValCell, HPos.CENTER);

					} else {
						ValueCellControl valCell = new ValueCellControl(startValue);
						valueCellControls.put(getIdentifier(x, y), valCell);
						gp.add(valCell, x, y);
					}

				} else {
					Pane r = new Pane();
					r.setStyle("-fx-background-color: blue");
					r.setMinSize(blockSize, blockSize);
					// do not use rectangle here, it is not able to stretch to
					// its parent boundary properly (sasc)
					// Rectangle r = new Rectangle(blockSize, blockSize,
					// Color.BLUE);
					gp.add(r, x, y);
				}

			}

		return gp;
	}

	/**
	 * This function should be called if the state (x,y) was updated. It changes
	 * the color and the label of the grid cell x, y according to value.
	 */
	private void updateValue(int x, int y, Value value) {
		Platform.runLater(() -> {
			ValueCellControl valCell = valueCellControls.get(getIdentifier(x, y));
			if (null != valCell)
				valCell.setValue(value.getValue());
		});
	}

	/**
	 * This function should be called if the state (x,y) was updated. It changes
	 * the color and the label of the grid cell x, y according to value. This
	 * function is only called if QLearning is selected.
	 * 
	 * @param x
	 *            cell in the grid in x direction
	 * @param y
	 *            cell in the grid in y direction
	 * @param valueN
	 *            value for the label in north direction
	 * @param valueS
	 *            value for the label in south direction
	 * @param valueW
	 *            value for the label in west direction
	 * @param valueE
	 *            value for the label in east direction
	 */
	private void updateQValues(int x, int y, Value valueN, Value valueS, Value valueW, Value valueE) {
		Platform.runLater(() -> {
			// this shouldn't be null here
			assert (allQValueCellControls != null);

			// color of background is updated while setting the value for the
			// label
			QValueCellControl qValueCell = allQValueCellControls.get(getIdentifier(x, y));
			if (null != qValueCell) {
				qValueCell.setValueUp(valueN.getValue());
				qValueCell.setValueDown(valueS.getValue());
				qValueCell.setValueLeft(valueW.getValue());
				qValueCell.setValueRight(valueE.getValue());
			}
		});

	}

	private ManualSimulationAgent manualAgent;

	/**
	 * Handles user input for the simulation. Use the arrow keys to move the pac
	 * in the simulation grid.
	 * 
	 * **/
	private void setupKeyHandler() {
		stage.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {

				// if we are currently editing, training or using autoplay of a
				// single episode the user actions should not affect the pac on
				// gamefield
				if (isEditing || isPlayingInBackground || isAutoplay)
					return;

				MoveAction desiredAction;
				switch (event.getCode()) {
				case UP:
					desiredAction = MoveAction.GO_NORTH;
					break;
				case RIGHT:
					desiredAction = MoveAction.GO_EAST;
					break;
				case DOWN:
					desiredAction = MoveAction.GO_SOUTH;
					break;
				case LEFT:
					desiredAction = MoveAction.GO_WEST;
					break;
				default:
					return;
				}

				if (env.getPac().willNotRunIntoAWallBy(desiredAction, env.getMaze())) {
					Agent oldAgent = currentTask.getAgent();
					manualAgent = new ManualSimulationAgent(oldAgent);
					manualAgent.setNext(desiredAction);
					currentTask.setAgent(manualAgent);
					env.performOneTimeStep();
					currentTask.setAgent(oldAgent);
				}
			}
		});

		// text fields loose focus if something in the window is clicked
		stage.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				EventTarget target = mouseEvent.getTarget();
				if (target instanceof Pane) {
					grid.requestFocus();
				}
			}
		});
	}

	/**
	 * clears the maze from remaining dots and repopulates them at the old
	 * positions
	 */
	private void resetDots() {
		Platform.runLater(() -> {
			// remove dotUI
			ObservableList<Node> childrens = grid.getChildren();
			for (DotUI dotUI : dotUIs) {
				childrens.remove(dotUI);
			}

			List<Dot> dots = env.getDotsForMarkingTerminalStates();
			for (Dot dot : dots) {
				// show the dot at the old position
				DotUI dotUI = new DotUI(dot, blockSize * 0.1, Color.GOLD);
				GridPane.setValignment(dotUI, VPos.CENTER);
				GridPane.setHalignment(dotUI, HPos.CENTER);
				Pane p = getValueCellPaneFromGridPane(grid, dot.getPosition().x, dot.getPosition().y);
				if (p != null) {
					p.getChildren().add(dotUI);
					dotUI.toBack();
					dotUIs.add(dotUI);
				}
			}
		});
	}

	/**
	 * clears the maze from remaining ghosts and repopulates them at the old
	 * positions
	 */
	private void resetGhosts() {
		Platform.runLater(() -> {
			// remove staticGhostUI
			ObservableList<Node> childrens = grid.getChildren();
			for (StaticGhostUI ui : ghostUIs) {
				childrens.remove(ui);
			}

			List<Point> staticGhosts = env.getStaticGhostsForMarkingTerminalStates();
			for (Point staticGhost : staticGhosts) {
				// paint the ghosts
				StaticGhostUI ghostUI = new StaticGhostUI(staticGhost, blockSize * 0.25);
				GridPane.setValignment(ghostUI, VPos.CENTER);
				GridPane.setHalignment(ghostUI, HPos.CENTER);
				Pane p = getValueCellPaneFromGridPane(grid, staticGhost.x, staticGhost.y);
				if (p != null) {
					p.getChildren().add(ghostUI);
					ghostUI.toBack();
					ghostUIs.add(ghostUI);
				}
			}
		});
	}

	/**
	 * removes the pac ui element from the maze grid and adds it again at the
	 * start position
	 * */
	private void resetPac() {
		Platform.runLater(() -> {
			if (grid.getChildren().contains(pacUI))
				grid.getChildren().remove(pacUI);
			Pac pac = env.getPac();
			pacUI = new PacUI(pac, pacRadius, Color.YELLOW, 1);
			GridPane.setValignment(pacUI, VPos.CENTER);
			GridPane.setHalignment(pacUI, HPos.CENTER);
			grid.add(pacUI, pac.getPosition().x, pac.getPosition().y);
		});
	}

	/**
	 * Sends the user back to menu.
	 * */
	@FXML
	public void cancel() {
		switchView(ViewFile.GAME_VIEW, ViewFile.MAINMENU_VIEW);
	}

	/**
	 * enabled editing mode or saves the changes depending on the current state
	 * of isEditing
	 * */
	@FXML
	public void editSave() {
		if (isEditing) {
			if (save()) {
				isEditing = false;
				enableDisableButtonsAndFields();
			}
		} else {
			isEditing = true;
			enableDisableButtonsAndFields();
		}
	}

	/**
	 * Disables or enabled the buttons and fields in simulation view depending
	 * on the state of the simulation which is defined by isEditing,
	 * isPlayingInBackground and isAutoplay <br>
	 * isEditing = true if we clicked the "Edit" button below the settings <br>
	 * isAutoplay = true if we are currently playing an episode by "doEpisode" <br>
	 * isPlayingInBackground = true if we are currently training <br>
	 * */
	private void enableDisableButtonsAndFields() {
		// Simulation and Training fields / buttons
		// can't use training if we are currently training or editing the values
		// or using doepisode
		btnDoTraining.setDisable(isEditing || isPlayingInBackground || isAutoplay);
		tfTrainingNumber.setDisable(isEditing || isPlayingInBackground || isAutoplay);
		btnDoEpisode.setDisable(isEditing || isPlayingInBackground || isAutoplay);
		// only available if we are playing an episode
		btnStopEpisode.setDisable(isEditing || isPlayingInBackground || !isAutoplay);
		btnDoStep.setDisable(isEditing || isPlayingInBackground || isAutoplay);

		// settings fields and buttons
		tfGamma.setDisable(!isEditing);
		tfAlpha.setDisable(!isEditing);

		ActionSelection actSelMethod = currentTask.getAgent().getPolicy().getActionSelectionMethod();
		if (actSelMethod instanceof GreedyActionSelection)
			tfEpsilon.setDisable(true);
		else
			tfEpsilon.setDisable(!isEditing);

		combbTrainingMethod.setDisable(!isEditing);
		combbSelectionPolicy.setDisable(!isEditing);
		combbMaze.setDisable(!isEditing);
		btnEditSave.setDisable(isPlayingInBackground || isAutoplay);
		// adjust the text on the button according to state
		btnEditSave.setText(isEditing ? SAVE_STRING : EDIT_STRING);
	}

	/**
	 * Saves the values in the textfields and comboboxes in the agent. create a
	 * new agent if necessary and update the ui.
	 * */
	public boolean save() {
		try {
			double discountRate = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfGamma, "Discount Rate");
			double learningRate = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfAlpha, "Learning Rate");
			
			// is ignored if not needed by action selection method
			String actionSelectionString = (String) combbSelectionPolicy.getSelectionModel().getSelectedItem();
			double epsilon = 0;
			switch(actionSelectionString){
			case ActionSelection.EPSGREEDY:
				 epsilon = TextfieldConverterHelper.getPositiveDoubleSmallerOne(tfEpsilon, "Epsilon");
				 break;
			case ActionSelection.SOFTMAX:
				 epsilon = TextfieldConverterHelper.getPositiveDoubleGreaterZero(tfEpsilon, "Temperature");
				 break;
			default:
				epsilon = TextfieldConverterHelper.getPositiveDouble(tfEpsilon, "Epsilon");
				break;
			}
			
			String mazeName = (String) combbMaze.getSelectionModel().getSelectedItem();
			String valueFuncString = (String) combbTrainingMethod.getSelectionModel().getSelectedItem();

			boolean mazeChanged = !mazeName.equals(currentTask.getMazeName());
			boolean valueFunctionChanged = !valueFuncString.equals(currentTask.getAgent().getPolicy().getValueFunction().getName());

			// value function or maze was changed, we need to create a new agent
			// and reset the ui
			if (mazeChanged || valueFunctionChanged) {

				// show message to the user that he will loose all his training
				// progress
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirmation Dialog");
				alert.setHeaderText("Are you sure?");
				alert.setContentText("You will loose all your training progress by changing value function or maze.");

				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() != ButtonType.OK) {
					// ... user chose CANCEL or closed the dialog
					// no changes will be made to the agent
					return false;
				}

				// we have to change the maze in environment
				if (mazeChanged) {
					changeMaze(mazeName);
				}

				// values are ignored if not needed by value function
				List<Action> initActions = SimulationEnvironment.ALL_ACTIONS_EXCEPT_DO_NOTHING;
				List<State> terminalStates = getTerminalStates();
				Map<Long, Map<Long, Value>> initialStateValues = getInitialStateValues();
				currentTask.setAgent(AgentFactory.createAgentWith(valueFuncString, actionSelectionString, epsilon, initActions, discountRate,
						learningRate, terminalStates, initialStateValues));

				currentTask.resetTraining();
				Platform.runLater(() -> {
					grid.requestFocus();
					restart();
				});
				return true;
			} else {
				// value function hasn't changed it is not necessary to create a
				// new agent. Just update the values.
				AgentFactory.updateAgentValueFunctionWith(currentTask.getAgent(), discountRate, learningRate);
				AgentFactory.updateAgentActionSelectionWith(currentTask.getAgent(), actionSelectionString, epsilon);
				return true;
			}

		} catch (Exception e) {
			showMessage("Message from PacMan", "Can't save Input Values", e.getMessage());
		}
		return false;
	}

	/**
	 * Return the initial state values of the current environment as a Map which
	 * is necessary to initialize special preterminal states
	 * */
	private Map<Long, Map<Long, Value>> getInitialStateValues() {

		// Init preterminal states s_i (a position where a dot is located) with
		// GO_NORTH action dna: Q(s_i, dna) = 0.0
		// Positive pre terminal states
		Map<Long, Map<Long, Value>> initialStateValues = new HashMap<>();
		for (Dot dot : env.getDotsForMarkingTerminalStates()) {
			Map<Long, Value> mapForPreterminalStateValues = new HashMap<>();
			mapForPreterminalStateValues.put(MoveAction.GO_NORTH.getSimpleId(), new Value());

			State state = new SimulationState(dot.getPosition());
			initialStateValues.put(state.getSimpleId(), mapForPreterminalStateValues);
		}
		// Negative pre terminal states
		for (Point point : env.getStaticGhostsForMarkingTerminalStates()) {
			Map<Long, Value> mapForPreterminalStateValues = new HashMap<>();
			mapForPreterminalStateValues.put(MoveAction.GO_NORTH.getSimpleId(), new Value());

			State state = new SimulationState(point);
			initialStateValues.put(state.getSimpleId(), mapForPreterminalStateValues);
		}
		return initialStateValues;
	}

	/**
	 * returns the terminal state of the simulation environment. the terminal
	 * state is an preassigned addition state. We need it because the positive
	 * and negative terminal states are only 'pre terminal' states.
	 * */
	private List<State> getTerminalStates() {
		List<State> terminalStates = new ArrayList<>(1);
		terminalStates.add(SimulationEnvironment.TERMINAL_STATE);
		return terminalStates;
	}

	/** Performs one time step */
	@FXML
	public void doStep() {
		env.performOneTimeStep();
	}

	/**
	 * performs one episode in a separate thread. while the episode is running
	 * it is possible to stop the episode or wait till it finished. But it is
	 * not possible to edit some values or interact with other buttons.
	 */
	@FXML
	public void doEpisode() {

		isAutoplay = true;
		enableDisableButtonsAndFields();

		Thread thread = new Thread() {
			public void run() {

				// perform steps until reaching end state
				env.performOneEpisode();
			}
		};

		thread.start();

	}

	/**
	 * stops a currently running episode and enabled the other buttons again
	 * */
	@FXML
	public void stopEpisode() {
		env.cancelEpisode();
		nextEpisode();
		isAutoplay = false;
		enableDisableButtonsAndFields();
	}

	/**
	 * disables or enabled the buttons of the ui
	 * 
	 * @param disable
	 *            true if the buttons should be disabled, false if they should
	 *            be enabled
	 * */
	public void disableButtons(boolean disable) {
		btnDoTraining.setDisable(disable);
		btnDoEpisode.setDisable(disable);
		btnStopEpisode.setDisable(disable);
		btnDoStep.setDisable(disable);
		btnEditSave.setDisable(disable);
	}

	private TrainingHelper trainingHelper;

	/**
	 * starts the training of the pac with a specified number of episodes
	 * (number is read from tfTrainingNumber). A progress dialog is shown to the
	 * user.
	 * */
	@FXML
	public void doTraining() {

		isPlayingInBackground = true;
		// we can't edit something or start a new training while we are
		// currently training
		enableDisableButtonsAndFields();

		// train a number of episodes
		int nrOfEpisodes = 0;
		try {
			nrOfEpisodes = Integer.parseInt(tfTrainingNumber.getText().trim());
			if (nrOfEpisodes < 1)
				throw new Exception();

		} catch (Exception e) {
			showMessage("Input Error", "Input Error", "Number of training episodes is not a valid value. Choose a positive integer.");
			return;
		}

		env.enableTrainingMode(true);
		env.removeEnvironmentChangedListener(this);
		trainingHelper = new TrainingHelper(currentTask, env, nrOfEpisodes, this);

		// Show dialog
		trainingHelper.getProgressBarDialog().setOnCloseRequest(new EventHandler<DialogEvent>() {
			public void handle(DialogEvent de) {
				// We have to cancel the Training
				log.info("Closing training dialog");
				trainingHelper.getTraining().cancel();
			}
		});
		trainingHelper.getProgressBarDialog().show();

		// Start training in a new thread
		Thread thread = new Thread(trainingHelper);
		thread.setName("Training");
		thread.start();
	}

	/**
	 * Is called by the training class as soon as the training has finished
	 * 
	 * @param canceled
	 *            true if the training process was canceled by the user, false
	 *            if all episodes where finished.
	 * @param trainingEpisodes
	 *            number of finished episodes
	 * */
	@Override
	public void trainingFinished(boolean canceled, int trainingEpisodes) {
		Platform.runLater(() -> {
			// enable buttons and fields after training finished
			isPlayingInBackground = false;
			enableDisableButtonsAndFields();

			env.enableTrainingMode(false);
			env.addEnvironmentChangedListener(this);

			updateAllLabels();

			// in case the user didn't close the dialog
			if (trainingHelper != null && trainingHelper.getProgressBarDialog() != null && trainingHelper.getProgressBarDialog().isShowing()) {
				trainingHelper.getProgressBarDialog().close();
			}

		});
	}

	/**
	 * Is called by the training class according to the progression of the
	 * training process. Updates the progress dialog shown to the user.
	 * 
	 * @param percent
	 *            a value between 0 and 100
	 * */
	@Override
	public void reportTrainingProgress(int percent) {
		trainingHelper.updateProgress(percent);
	}

	/**
	 * After training has finished this method updates the labels of the ui,
	 * resets the dots, ghost and pac.
	 * */
	private void updateAllLabels() {
		// EpisodesLabel
		updateEpisodesLabel();

		// Ghost, Pac, and Dots
		resetDots();
		resetGhosts();
		resetPac();

		// Values
		updateAllValues();

	}

	/**
	 * Updates the state values in the maze grid on the ui during the simulation
	 * progress.
	 * */
	private void updateAllValues() {

		for (int x = 0; x < maze.getWidth(); x++)
			for (int y = 0; y < maze.getHeight(); y++) {
				State s = new SimulationState(maze.getPointAt(x, y));
				stateValueChanged(s);
			}
	}

	/**
	 * Is called by the pac after the desired action (move in a specific
	 * direction) was executed. Updates the UI and displays the pac at the new
	 * position.
	 * */
	@Override
	public void objectMoved(MovableObject movableObject, boolean hasNewPosition, boolean isTeleport) {

		// nothing to show in training mode, so cancel here
		if (env.isInTrainingMode())
			return;

		if (movableObject instanceof Pac) {
			Point newPos = movableObject.getPosition();
			updatePacUIPos(newPos);
		} else
			throw new RuntimeException("SimulationController objectMoved moved object unknown type");
	}

	/** moves the pac to the new position */
	private void updatePacUIPos(Point newPos) {
		Platform.runLater(() -> {
			grid.getChildren().remove(pacUI);
			grid.add(pacUI, newPos.x, newPos.y);
		});
	}

	/**
	 * @return the custom ValueCellControl included in the gridpane at the given
	 *         position
	 */
	private ValueCellControl getValueCellPaneFromGridPane(GridPane gridPane, int col, int row) {
		for (Node node : gridPane.getChildren()) {
			if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row)
				if (node instanceof ValueCellControl)
					return (ValueCellControl) node;
		}
		return null;
	}

	/**
	 * resets the pac, dot and the state of environment. should be called after
	 * the dot was eaten by the pac.
	 * */
	private void nextEpisode() {
		resetPac();
		resetDots();
		resetGhosts();
	}

	private void restart() {
		env.reset();
		loadGrid();
		lblCurrentEpisodes.setText("0");
	}

	private Value getValue(int x, int y, Action a) {
		return env.getValue(x, y, a);
	}

	@Override
	public void lastDotWasEaten(Dot eatenDot, Integer score) {
		// nothing to do here
	}

	@Override
	public void pacWasCaughtBy(Ghost ghost) {
		// nothing to do here
	}

	@Override
	public void dotWasEaten(Dot eatenDot) {
		// nothing to do here
	}

	/**
	 * Is used to store all CellControls in a list with a unique id
	 * 
	 * @return an unique identifier given for the coordinates
	 * */
	private long getIdentifier(int x, int y) {
		long id = ((long) x) << 32;
		id = id | y;
		return id;
	}


	/** We need to remember previous state in Sarsa-Learning since we update one iteration delayed. */
	SimulationState previousSarsaState = null;

	/**
	 * Is called by the SimulationEnvironment after the Reward was distributed
	 * to the ValueFunction and the value of the last state was updated. Now we
	 * are able to update the label in the grid and show the new value.
	 * */
	@Override
	public void stateValueChanged(State s) {

		if (s instanceof SimulationState) {
			// we have to update the state value on the label and maybe need to
			// change the color of the background
			SimulationState state = (SimulationState) s;
			ValueFunction vf = currentTask.getAgent().getPolicy().getValueFunction();

			if (vf instanceof QLearning) {

				updateQLearningState(state);
				// Sarsa need also to update the previous state since we update one iteration delayed.
				if (vf instanceof Sarsa) {
					if (null != previousSarsaState)
						updateQLearningState(previousSarsaState);
					previousSarsaState = state;
				}

			} else {
				// last param is null because we dont have any action to pass
				// we only have state action pairs in qvalueIteration
				Value value = getValue(state.getX(), state.getY(), null);
				updateValue(state.getX(), state.getY(), value);

			}
		}

	}

	private void updateQLearningState(SimulationState state) {
		// note: preterminal states doesn't have state-action value
		// labels, because we only have one value for this state (no
		// more actions possible afterwards)
		if (env.isPreTerminalState(state)) {
			// Action GO_NORTH because this is the arbitrary action in
			// env.rewardPacForReachingTerminalState()
			Value value = getValue(state.getX(), state.getY(), MoveAction.GO_NORTH);
			updateValue(state.getX(), state.getY(), value);
		} else {
			Value valueN = getValue(state.getX(), state.getY(), MoveAction.GO_NORTH);
			Value valueS = getValue(state.getX(), state.getY(), MoveAction.GO_SOUTH);
			Value valueW = getValue(state.getX(), state.getY(), MoveAction.GO_WEST);
			Value valueE = getValue(state.getX(), state.getY(), MoveAction.GO_EAST);

			updateQValues(state.getX(), state.getY(), valueN, valueS, valueW, valueE);
		}
	}

	/**
	 * is called by SimulationEnvironment as soon as an episode has finished.
	 * Resets and updates the ui elements.
	 * 
	 * @param canceled
	 *            is true if the episode was canceled by the user
	 * @param steps
	 *            the number of time steps needed to finish this episode
	 * */
	@Override
	public void episodeWasFinished(boolean canceled, int steps) {
		if (!canceled)
			updateEpisodesLabel();

		if (env.isInTrainingMode())
			return;

		nextEpisode();

		// for enabled fields after doEpisode finished
		isAutoplay = false;
		enableDisableButtonsAndFields();
	}

	private void updateEpisodesLabel() {
		Platform.runLater(() -> {
			int currentEpisodes = currentTask.getCurrentTrainingLevel();
			lblCurrentEpisodes.setText(Integer.toString(currentEpisodes));
		});
	}

	@Override
	public void gameOver(Integer score) {
		// Nothing to do here
	}

	@Override
	public void populatedDots() {
		// Nothing to do here
	}

}
