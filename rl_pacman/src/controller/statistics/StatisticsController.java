package controller.statistics;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import controller.Controller;
import controller.statistics.ChartFactory.ChartType;
import view.StatisticsTableControl;
import model.Task;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

/**
 * Controller for Statistic View. Displays the statistic values of the current
 * selected task(s) to the user. Offers a table and chart view for better
 * comparison of the data.
 * */
public class StatisticsController extends Controller implements Initializable {

	/** holds all statistic table controls. one for each selected task. **/
	@FXML
	private HBox hboxTaskTables;
	
	@FXML
	private AnchorPane mainAnchorPane;

	/** displays a message to the user if necessary. */
	@FXML
	private Label lblMessage;

	/** combobox with the different chart types e.g. average score by level */
	@FXML
	private ComboBox<String> combbChartTypes;

	/** tasks to compare which where selected by the user */
	private List<Task> tasks = new ArrayList<>();

	/** Remembers the current selected chart type. */
	private String currentChartType = ChartFactory.DEFAULT_CHART_TYPE.getName();

	/**
	 * is called while initialising the view. it would be possible to add
	 * dynamically created ui elements here before displaying anything to the
	 * user. in case of this view we need our settings object, but the settings
	 * object is not accessible in initialize because creating the view is not
	 * completed. therefore we use the afterInit method, which is called right
	 * after initialisation of the view has finished.
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {

	}

	/**
	 * is called right after initialisation of the view has finished. We use the
	 * settings object to access the statistic data we need. depending on this
	 * statistic data the tables are created and displayed to the user.
	 * */
	@Override
	public void afterInit() {

		tasks = settings.getSelectedTasks();

		// add chart types and select the default entry
		combbChartTypes.setItems(ChartType.getChartTypesAsList());
		combbChartTypes.setValue(currentChartType);
		combbChartTypes.getSelectionModel().selectedItemProperty().addListener(new ChartTypeChangedListener());

		// no data to display -> stop here
		if (tasks.size() <= 0) {
			lblMessage.setText("No task data available");
			return;
		}

		lblMessage.setText("");

		showTable();

	}

	/**
	 * Displays a new chart if the user selected a different chart type from the
	 * combobox
	 */
	private class ChartTypeChangedListener implements ChangeListener<String> {
		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			if (newValue == null)
				return;
			// remember the user decision
			currentChartType = newValue;
			// updates the chart view
			showChart();
		}
	}

	/**
	 * updates the statistic tables in the ui. removes the old tables first.
	 * */
	@FXML
	public void showTable() {
		combbChartTypes.setDisable(true);
		// remove previous tables or charts or whatever
		hboxTaskTables.getChildren().clear();
		StatisticsTableControl curControl;
		
		//create a table for each task
		for (Task t : tasks) {
			curControl = new StatisticsTableControl(t);
			hboxTaskTables.getChildren().add(curControl);
		}
	}

	/** displays a chart with some statistic data to the user.*/
	@FXML
	public void showChart() {
		combbChartTypes.setDisable(false);
		//
		double chartWidth = mainAnchorPane.getWidth() * 0.8;
		double chartHeight =  mainAnchorPane.getHeight() * 0.7;
		LineChart<Number, Number> currentChart = ChartFactory.createLineChart(chartWidth, chartHeight, tasks, currentChartType);
		if (currentChart != null) {
			// remove previous tables or charts or whatever
			hboxTaskTables.getChildren().clear();
			hboxTaskTables.getChildren().add(currentChart);
		}
	}

	/** Sends the user back to the task overview. */
	@FXML
	public void back() {
		switchView(ViewFile.STATISTICS_VIEW, ViewFile.TASKOVERVIEW_VIEW);
	}

}