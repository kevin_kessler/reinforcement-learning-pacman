package controller.timing;

import java.util.Timer;
import java.util.TimerTask;

public class GameTimer {
	
	private boolean isRunning;
	private Timer timer;
	private TimerFirable firable;
	private GameTimerTask timerTask;
	private long timePeriodInMillisec;
	
	public GameTimer(long timePeriodInMillisec, TimerFirable firable) {
		assert(null != firable);
		initTimer(timePeriodInMillisec, firable);
	}
	
	private void initTimer(long timePeriodInMillisec, TimerFirable firable) {
		this.firable = firable;
		this.timerTask = new GameTimerTask(firable);
		this.timePeriodInMillisec = timePeriodInMillisec;
		this.isRunning = false;
	}
	
	public void start() {
		if (isRunning) 
			stop();
		initTimer(timePeriodInMillisec, firable);
		timer = new Timer();
		timer.schedule(timerTask, 0, timePeriodInMillisec);
		isRunning = true;
	}
	public void stop() {
		if (null != timer) {
			timer.cancel();
			timer = null;
			isRunning = false;
		}
	}
	
	private static class GameTimerTask extends TimerTask {
		
		private final TimerFirable firable;
		
		GameTimerTask(TimerFirable f) {
			assert(null != f);
			this.firable = f;
		}

		@Override
		public void run() {
			firable.fire();
		}
	}
	
	public boolean isRunning(){
		return this.isRunning;
	}
}
