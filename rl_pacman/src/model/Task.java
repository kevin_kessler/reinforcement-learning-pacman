package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.reinforcement_learning.Agent;
import model.reinforcement_learning.AgentFactory;
import model.reinforcement_learning.Reward;
import model.environment.pacman.PacEnvironment.DotMode;
import utils.MazeLoader;
import utils.TaskPersistenceManager;

/**
 * This class represents a complete setup for reinforcement learning.<br/>
 * The setup includes a (possibly trained) pac agent with a specific policy
 * (value function and action selection method), ghost configuration, maze
 * (where the training was carried out), a specific set of penalties and rewards
 * for 'events' (such as game won, pac hit the wall, ...) and a list of
 * statistic data (one statistics object for each training level)
 *
 */
public class Task implements Serializable {

	private static final long serialVersionUID = -7793814773993722779L;

	private String name = "Default";
	private Agent agent;
	private String mazeName;
	private boolean blinkySelected = false;
	private boolean inkySelected = false;
	private boolean pinkySelected = false;
	private boolean clydeSelected = false;

	private Reward deathPenalty = new Reward(-1000);
	private Reward stepPenalty = new Reward(-0.1);
	private Reward wonReward = new Reward(1000);
	private Reward dotReward = new Reward(40);
	private Reward wallHitPenalty = new Reward(-100);

	private DotMode dotMode = DotMode.OneDot;

	/**
	 * Represents the number of trained episodes of the agent.
	 */
	private int currentTrainingLevel;
	/**
	 * A list of statistics. One statistic belongs to a specific training level.
	 * A training level represents the number of trained episodes. The
	 * statistics should not be mixed up with a different level
	 */
	private List<Statistics> statisticsPerTrainingLevel = new ArrayList<>();

	public Task() {
		agent = AgentFactory.getDefaultApproxQLearningAgent();
		mazeName = MazeLoader.getInstance().getFirstPacMazeName();
	}

	public Task(Task taskToCopy) {
		this.name = "Copy of " + taskToCopy.name;
		this.agent = AgentFactory.copy(taskToCopy.agent);
		this.mazeName = taskToCopy.mazeName;
		this.blinkySelected = taskToCopy.blinkySelected;
		this.inkySelected = taskToCopy.inkySelected;
		this.pinkySelected = taskToCopy.pinkySelected;
		this.clydeSelected = taskToCopy.clydeSelected;

		this.deathPenalty = new Reward(taskToCopy.deathPenalty);
		this.stepPenalty = new Reward(taskToCopy.stepPenalty);
		this.wonReward = new Reward(taskToCopy.wonReward);
		this.dotReward = new Reward(taskToCopy.dotReward);
		this.wallHitPenalty = new Reward(taskToCopy.wallHitPenalty);

		this.dotMode = taskToCopy.dotMode;
		this.currentTrainingLevel = taskToCopy.currentTrainingLevel;

		for (Statistics s : taskToCopy.statisticsPerTrainingLevel) {
			this.statisticsPerTrainingLevel.add(new Statistics(s));
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public void save() {
		TaskPersistenceManager.getInstance().saveTask(this);
	}

	public void delete() {
		TaskPersistenceManager.getInstance().deleteTask(this);
	}

	public void overwrite() {
		TaskPersistenceManager.getInstance().overwriteTask(this);
	}

	public Agent getAgent() {
		return agent;
	}

	public Statistics getStatisticsForCurrentTrainingLevel() {
		return getStatisticsFor(currentTrainingLevel);
	}

	public Statistics getStatisticsFor(int nrOfTrainedEpisodes) {
		for (Statistics stat : statisticsPerTrainingLevel) {
			if (stat.NR_TRAINED_EPISODES == nrOfTrainedEpisodes) {
				return stat;
			}
		}
		// No statistics yet for this number of trained episodes
		Statistics newStat = new Statistics(nrOfTrainedEpisodes);
		statisticsPerTrainingLevel.add(newStat);
		return newStat;
	}

	/**
	 * @return the mazeFile
	 */
	public String getMazeName() {
		return mazeName;
	}

	/**
	 * @param mazeName
	 *            the mazeName to set
	 */
	public void setMazeName(String mazeName) {
		this.mazeName = mazeName;
	}

	/**
	 * @return the currentTrainingLevel for task overview
	 */
	public int getCurrentTrainingLevel() {
		return currentTrainingLevel;
	}

	/**
	 * @param currentTrainingLevel
	 *            the currentTrainingLevel to set
	 */
	public void setCurrentTrainingLevel(int currentTrainingLevel) {
		this.currentTrainingLevel = currentTrainingLevel;
	}

	public void addToTrainingLevel(int nrOfEpisodes) {
		// Assert that no overflow will occur
		assert ((1L * nrOfEpisodes + this.getCurrentTrainingLevel()) < (long) Integer.MAX_VALUE);
		this.setCurrentTrainingLevel(nrOfEpisodes + this.getCurrentTrainingLevel());
	}

	/**
	 * @return the blinkySelected
	 */
	public boolean isBlinkySelected() {
		return blinkySelected;
	}

	/**
	 * @param blinkySelected
	 *            the blinkySelected to set
	 */
	public void setBlinkySelected(boolean blinkySelected) {
		this.blinkySelected = blinkySelected;
	}

	/**
	 * @return the inkySelected
	 */
	public boolean isInkySelected() {
		return inkySelected;
	}

	/**
	 * @param inkySelected
	 *            the inkySelected to set
	 */
	public void setInkySelected(boolean inkySelected) {
		this.inkySelected = inkySelected;
	}

	/**
	 * @return the pinkySelected
	 */
	public boolean isPinkySelected() {
		return pinkySelected;
	}

	/**
	 * @param pinkySelected
	 *            the pinkySelected to set
	 */
	public void setPinkySelected(boolean pinkySelected) {
		this.pinkySelected = pinkySelected;
	}

	/**
	 * @return the clydeSelected
	 */
	public boolean isClydeSelected() {
		return clydeSelected;
	}

	/**
	 * @param clydeSelected
	 *            the clydeSelected to set
	 */
	public void setClydeSelected(boolean clydeSelected) {
		this.clydeSelected = clydeSelected;
	}

	/**
	 * @return the deathPenalty
	 */
	public Reward getDeathPenalty() {
		return deathPenalty;
	}

	/**
	 * @param deathPenalty
	 *            the deathPenalty to set
	 */
	public void setDeathPenalty(float deathPenalty) {
		this.deathPenalty = new Reward(deathPenalty);
	}

	public Reward getStepPenalty() {
		return stepPenalty;
	}

	public void setStepPenalty(float stepPenalty) {
		this.stepPenalty = new Reward(stepPenalty);
	}

	/**
	 * @return the wonReward
	 */
	public Reward getWonReward() {
		return wonReward;
	}

	/**
	 * @param wonReward
	 *            the wonReward to set
	 */
	public void setWonReward(float wonReward) {
		this.wonReward = new Reward(wonReward);
	}

	/**
	 * @return the dotReward
	 */
	public Reward getDotReward() {
		return dotReward;
	}

	/**
	 * @param dotReward
	 *            the dotReward to set
	 */
	public void setDotReward(float dotReward) {
		this.dotReward = new Reward(dotReward);
	}

	/**
	 * @return the wallHitPenalty
	 */
	public Reward getWallHitPenalty() {
		return wallHitPenalty;
	}

	/**
	 * @param wallHitPenalty
	 *            the wallHitPenalty to set
	 */
	public void setWallHitPenalty(float wallHitPenalty) {
		this.wallHitPenalty = new Reward(wallHitPenalty);
	}

	/**
	 * @return the dotMode
	 */
	public DotMode getDotMode() {
		return dotMode;
	}

	/**
	 * @param dotMode
	 *            the dotMode to set
	 */
	public void setDotMode(DotMode dotMode) {
		this.dotMode = dotMode;
	}

	/**
	 * @return the statisticsPerTrainingLevel
	 */
	public List<Statistics> getStatisticsPerTrainingLevel() {
		return statisticsPerTrainingLevel;
	}

	/**
	 * @param statisticsPerTrainingLevel
	 *            the statisticsPerTrainingLevel to set
	 */
	public void setStatisticsPerTrainingLevel(List<Statistics> statisticsPerTrainingLevel) {
		this.statisticsPerTrainingLevel = statisticsPerTrainingLevel;
	}

	public void resetTraining() {
		statisticsPerTrainingLevel = new ArrayList<Statistics>();
		currentTrainingLevel = 0;
	}

}
