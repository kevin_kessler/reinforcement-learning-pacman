package model.environment;

import model.environment.pacman.Dot;
import model.environment.pacman.Ghost;
import model.environment.pacman.MovableObject;
import model.reinforcement_learning.State;

/**
 * The purpose of this interface is to notify the Environment UI when changes
 * occur in the models which have a reference to this listener. This interface
 * should be implemented by EnvironmentControllers which update the environment
 * specific UI. The model notifies the controller over several environment
 * specific events like game over, game won, object moved etc.
 * Thus the controller can notify the view to draw the changes.
 */
public interface EnvironmentChangeListener {

	/**
	 * Is called by the model when a dot has been eaten in the backend.
	 */
	void dotWasEaten(Dot eatenDot);

	/**
	 * Is called by the model when the last dot has been eaten in the backend.
	 * This equals the situation of winning the game.
	 */
	void lastDotWasEaten(Dot eatenDot, Integer score);

	/**
	 * Is called when the pac has been caught by a ghost in the backend. This is
	 * not equal to the game over situation, since the pac has several lives.
	 */
	void pacWasCaughtBy(Ghost ghost);

	/**
	 * Is called when an object took a move action within the environment.
	 * @param movableObject
	 * 			the object that took the action / tried to move.
	 * @param hasNewPosition
	 * 			whether the object changed it's position by performing the action.
	 * @param isTeleport
	 * 			whether the object moved to a neighboring field or to a field further away.
	 */
	void objectMoved(MovableObject movableObject, boolean hasNewPosition, boolean isTeleport);

	/**
	 * Is called when the value of a certain state changed.
	 * @param state
	 * 			the state whose value has been changed
	 */
	void stateValueChanged(State state);

	/**
	 * Is called when the current episode ended.
	 * This is usually used to update a progressbar that shows the progress
	 * of performing several episodes.
	 * @param canceled
	 * 			whether the episode has been cancelled.
	 * @param steps
	 * 			the number of steps taken in this episode.
	 */
	void episodeWasFinished(boolean canceled, int steps);

	/**
	 * Is called when the current game ended (usually by losing the last life).
	 * @param score
	 * 			The game score reached before game was over.
	 */
	void gameOver(Integer score);

	/**
	 * Is called when the dots of the game has been populated / reset.
	 * Should be used to repaint the dot UIs.
	 */
	void populatedDots();
}
