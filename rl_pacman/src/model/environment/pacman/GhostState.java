/**
 * 
 */
package model.environment.pacman;

/**
 * HUNTING = Ghost is hunting pacman
 * EXITING = Indicates whether the ghost is presently exiting the box
 * INJAIL = Ghost is currently in jail and waiting 
 */
public enum GhostState {
	HUNTING, EXITING, INJAIL; 
}
