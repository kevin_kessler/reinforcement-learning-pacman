package model.environment.pacman;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Logger;

import model.Task;
import model.environment.EnvironmentChangeListener;
import model.environment.MoveAction;
import model.reinforcement_learning.Action;
import model.reinforcement_learning.Environment;
import model.reinforcement_learning.Reward;
import model.reinforcement_learning.State;
import model.reinforcement_learning.value_function.ValueParams;
import utils.MazeLoader;

/**
 * Represents the environment of the reinforcement learning task. Holds the
 * model information for pac, ghosts, dots and the maze. The purpose of the
 * environment is to provide states and change the current state according to
 * the actions of the agent. Moreover it delivers the rewards to the agent, thus
 * the reinforcement learning is possible.
 * **/
public class PacEnvironment implements Environment {

	protected final static Logger log = Logger.getLogger(PacEnvironment.class.getName());

	public static final List<Action> ALL_ACTIONS = Collections.unmodifiableList(new ArrayList<Action>(EnumSet.allOf(MoveAction.class)));
	public static final List<Action> ALL_ACTIONS_EXCEPT_DO_NOTHING;

	static {
		ArrayList<Action> tmpActionsEceptNothing = new ArrayList<>();
		for (MoveAction a : EnumSet.allOf(MoveAction.class)) {
			if (a != MoveAction.DO_NOTHING)
				tmpActionsEceptNothing.add(a);
		}

		ALL_ACTIONS_EXCEPT_DO_NOTHING = Collections.unmodifiableList(tmpActionsEceptNothing);
	}

	public enum DotMode {
		OneDot("One"), AllDots("All");

		public final static List<DotMode> ALL_DOT_MODES = Collections.unmodifiableList(new ArrayList<DotMode>(EnumSet.allOf(DotMode.class)));

		private String label;

		DotMode(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static DotMode getDotModeByString(String dotModeName) {
			for (DotMode d : ALL_DOT_MODES) {
				if (d.getLabel().equals(dotModeName))
					return d;
			}
			return null;
		}
	}

	private final List<EnvironmentChangeListener> envChangedListeners = new ArrayList<>();

	private Pac pac;
	private List<Ghost> ghosts = new ArrayList<Ghost>();
	private List<Dot> dots = new ArrayList<Dot>();
	private Maze maze;

	// Flags to determine reward and save game state
	private boolean won = false;
	private boolean ateDot = false;
	private boolean hitGhost = false;
	private boolean gameOver = false;
	private boolean walkedAgainstAWall = false;

	// allows us to cancel an episode e.g. if training is canceled
	protected boolean episodeCanceled = false;

	private Task currentTask;
	/**
	 * Decides whether an episode is currently being executed or not. It ends
	 * when the last dot was eaten or or pac was caught by a ghost and lost his
	 * last life or when the user decides to stop it.
	 */
	private boolean episodeEnded = false;

	/**
	 * If we are in learning mode then the agent will get rewards and update his
	 * value function.
	 */
	private boolean isInLearningMode = false;

	/**
	 * If we are in Training mode then no progress is visually shown to the
	 * user. we are only in traning mode if the user hit the "training" button.
	 * */
	private boolean isInTrainingMode = false;

	private boolean resetNecessary = false;
	private boolean restartNecessary = false;

	public PacEnvironment(Task task) {
		this.currentTask = task;
		this.maze = MazeLoader.getInstance().getPacMaze(currentTask.getMazeName());
		setPac();
		setGhosts();
		reset();
	}

	public PacEnvironment(PacEnvironment peToCopy) {
		this(new Task(peToCopy.currentTask));
	}

	public PacEnvironment getCopy() {
		return new PacEnvironment(this);
	}

	private void setGhosts() {
		// note that the ghosts will have different waiting periods (Thus they
		// will not leave the jail at the same time).
		// blinky will initially have no waiting timer. (see blinky class)
		Blinky blinky = null;
		if (currentTask.isBlinkySelected()) {
			blinky = new Blinky(this);
			addGhost(blinky);
			blinky.setWaitingPeriod(ghosts.size() * Ghost.GHOST_WAITING_PERIOD_DELAY);
		}
		if (currentTask.isPinkySelected()) {
			Pinky pinky = new Pinky(this);
			addGhost(pinky);
			pinky.setWaitingPeriod(ghosts.size() * Ghost.GHOST_WAITING_PERIOD_DELAY);
		}
		// Inky needs blinky
		if (currentTask.isInkySelected() && currentTask.isBlinkySelected()) {
			Inky inky = new Inky(blinky, this);
			addGhost(inky);
			inky.setWaitingPeriod(ghosts.size() * Ghost.GHOST_WAITING_PERIOD_DELAY);
		}

		if (currentTask.isClydeSelected()) {
			Clyde clyde = new Clyde(this);
			addGhost(clyde);
			clyde.setWaitingPeriod(ghosts.size() * Ghost.GHOST_WAITING_PERIOD_DELAY);
		}
	}

	private void setPac() {
		pac = new Pac(maze.getPacSpawnPos());
		pac.setListeners(envChangedListeners);
	}

	public void addGhost(Ghost ghost) {
		this.ghosts.add(ghost);
		ghost.setListeners(envChangedListeners);
	}

	public Pac getPac() {
		return this.pac;
	}

	public Maze getMaze() {
		return this.maze;
	}

	public List<Ghost> getGhosts() {
		return ghosts;
	}

	public List<Dot> getDots() {
		return dots;
	}

	public void setDots(List<Dot> dots) {
		this.dots = dots;
	}

	public void setGhosts(List<Ghost> ghosts) {
		this.ghosts = ghosts;
	}

	public void setMaze(Maze maze) {
		this.maze = maze;
	}

	/**
	 * Populates the list of dots by adding dots according to the given dotMode
	 */
	private void populateDots(DotMode dotMode) {
		switch (dotMode) {

		// Populates the list of dots by adding one dot for each walkable
		// position of the maze.
		case AllDots:
			int height = maze.getHeight();
			int width = maze.getWidth();

			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					Maze.BlockType bt = maze.getBlockTypeAt(j, i);
					if (maze.isWalkable(bt) && !maze.isGhostArea(bt) && !maze.isPacSpawn(bt)) {
						dots.add(new Dot(maze.getPointAt(j, i)));
					}
				}
			}
			break;

		// Populates the list of dots by adding one dot at one random walkable
		// position of the maze.
		case OneDot:
			Point p = maze.getRandomWalkablePosition();
			dots.add(new Dot(p));
			break;

		default:
			throw new RuntimeException("DotMode " + dotMode + " not supported!");
		}

		notifyAboutDotPopulation();
	}

	/**
	 * This function will be used to train the agent one episode.
	 */
	@Override
	public void performOneEpisode() {
		episodeEnded = false;
		episodeCanceled = false;

		while (!episodeEnded && !episodeCanceled) {
			performOneTimeStep();
		}
	}

	public void cancelEpisode() {
		episodeCanceled = true;
	}

	/**
	 * One time step represents the transition from t to t+1. Invokes the
	 * RL-agent and executes received action.
	 */
	public void performOneTimeStep() {
		State currentState = getCurrentState();
		List<Action> currentPossibleActions = ALL_ACTIONS_EXCEPT_DO_NOTHING;
		Action nextAction = currentTask.getAgent().determineNextAction(currentState, currentPossibleActions);
		executeAction(nextAction);
		if (isInLearningMode()) {
			Reward reward = getRewardForLastExecutedAction();
			State successorState = getCurrentState();
			currentTask.getAgent().rewardLastAction(new ValueParams(currentState, nextAction, reward, successorState));
		}
		resetGamestateFlags();
	}

	/**
	 * Executes the given action. Caution, this results in a transition from t
	 * to t+1. This should only be called from a controller for a human player.
	 * 
	 * @param nextAction
	 */
	public void executeAction(Action nextAction) {
		if (resetNecessary) {
			reset();
			resetNecessary = false;
		} else if (restartNecessary) {
			restart();
			restartNecessary = false;
		}

		Point oldPos = pac.getPosition();
		// Send action to Pac
		pac.performAction(nextAction, this.maze);
		// if we didn't moved at all we assume pac walked against a wall
		if (oldPos.equals(pac.getPosition()))
			walkedAgainstAWall = true;

		checkIfDotWasEatenAndResetIfNecessary();

		// if pac ate the last dot, he wins, even if a ghost is at the same spot
		// resetNecessary is true when lastDotWasEaten in
		// checkIfDotWasEatenAndAdjustScore()
		if (resetNecessary) // equals a win
			return;

		// check for collision before ghosts moved
		boolean hitAlready = hittestBetweenPacAndGhosts();
		if (hitAlready)
			return;

		// move ghosts and check for collision one more time
		moveGhosts();
		hittestBetweenPacAndGhosts();

	}

	/**
	 * Moves all ghosts. To realize slower movement we need to split pac
	 * movement from ghost movement by using this function.
	 */
	public void moveGhosts() {
		for (Ghost ghost : ghosts) {
			ghost.move();
		}
	}

	/**
	 * Checks whether one or more ghosts share their position with the pac. In
	 * case of collision pac lives are decremented. In case of game over episode
	 * is ended.
	 * 
	 * @return true if they hit each other, false otherwise.
	 */
	private boolean hittestBetweenPacAndGhosts() {
		Ghost ghostWhichCaughtPac = null;
		for (Ghost g : ghosts) {
			if (g.getPosition().equals(pac.getPosition())) {
				// The current ghost touched pac
				ghostWhichCaughtPac = g;
				break; // We can break the loop since one ghost touch is enough
			}
		}
		if (null != ghostWhichCaughtPac) {
			pac.decrementLives();
			hitGhost = true;
			if (pac.getLives() < 1)
				gameOver = true;

			if (gameOver) {
				endEpisode();
				notifyAboutGameOver();
				resetNecessary = true;
			} else {
				restartNecessary = true;
				notifyPacWasCaughtBy(ghostWhichCaughtPac);
			}

			return true;
		}
		return false;

	}

	/**
	 * Checks whether or not the pac is in the same spot with a dot. This dot
	 * will be consumed and removed from the dot-list. Moreover all listeners
	 * are notified about this event (GameController should update the ui). If
	 * the last dot was eaten the flags won and resetNecessary will be set to
	 * true and the game will end.
	 * */
	private void checkIfDotWasEatenAndResetIfNecessary() {
		// Check if pac is on the same coordinate as a dot
		Dot eatenDot = null;
		for (Dot p : dots) {
			if (p.getPosition().equals(pac.getPosition())) {
				// We have an eaten dot
				eatenDot = p;
				break; // We can break the loop since we should not have another
						// dot on pac's position
			}
		}
		if (null != eatenDot) {
			dots.remove(eatenDot);
			pac.eat(eatenDot);
			if (dots.isEmpty()) {
				won = true;
				notifyAboutLastEatenDot(eatenDot);
				endEpisode();
				resetNecessary = true;
			} else {
				ateDot = true;
				notifyAboutEatenDot(eatenDot);
			}
		}

	}

	/**
	 * checks if the pac executed a action leading to a situation where a
	 * pre-defined reward or penalty should be distributed to the agent. Typical
	 * rewards are distributed for winning the game or eating a dot. Penalties
	 * are distributed for hitting the wall, dying or stepping into an empty
	 * spot. Rewards and Penalties are defined by the task (see task config)
	 */
	private Reward getRewardForLastExecutedAction() {
		Reward r;
		if (ateDot)
			r = currentTask.getDotReward();
		else if (walkedAgainstAWall)
			r = currentTask.getWallHitPenalty();
		else if (hitGhost)
			r = currentTask.getDeathPenalty();
		else if (won)
			r = currentTask.getWonReward();

		else
			// Otherwise assume a step into an empty spot
			r = currentTask.getStepPenalty();

		return r;
	}

	private void resetGamestateFlags() {
		won = false;
		ateDot = false;
		hitGhost = false;
		walkedAgainstAWall = false;
		gameOver = false;
	}

	public void addEnvironmentChangedListener(EnvironmentChangeListener listener) {
		if (!envChangedListeners.contains(listener))
			envChangedListeners.add(listener);
	}

	public void removeEnvironmentChangedListener(EnvironmentChangeListener listener) {
		envChangedListeners.remove(listener);
	}

	private void notifyAboutLastEatenDot(Dot eatenDot) {
		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.lastDotWasEaten(eatenDot, pac.getScore());
		}
	}

	private void notifyAboutEatenDot(Dot eatenDot) {
		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.dotWasEaten(eatenDot);
		}
	}

	private void notifyAboutDotPopulation() {
		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.populatedDots();
		}
	}

	private void notifyPacWasCaughtBy(Ghost ghostWhichCaughtPac) {
		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.pacWasCaughtBy(ghostWhichCaughtPac);
		}
	}

	private void notifyAboutGameOver() {
		for (EnvironmentChangeListener listener : envChangedListeners) {
			listener.gameOver(pac.getScore());
		}
	}

	/**
	 * will start a new episode. therefore all remaining dots will be removed
	 * and new dot(s) will be populated. also pac and ghosts will be reset to
	 * start positions.
	 * */
	private void reset() {
		getDots().clear();
		populateDots(currentTask.getDotMode());
		getPac().reset();
		resetGhosts();
	}

	/**
	 * Will reset the ghosts and pac to the start positions (for example if pac
	 * was eaten by a ghost)
	 * */
	private void restart() {
		getPac().setToSpawn();
		getPac().invalidateLastAction();
		resetGhosts();
	}

	private void resetGhosts() {
		for (Ghost g : getGhosts())
			g.reset();
	}

	public State getCurrentState() {
		return new PacManState(this, ALL_ACTIONS_EXCEPT_DO_NOTHING);
	}

	@Override
	public void enableLearning(boolean shouldTrain) {
		isInLearningMode = shouldTrain;
	}

	@Override
	public boolean isInLearningMode() {
		return isInLearningMode;
	}

	@Override
	public boolean isInTrainingMode() {
		return isInTrainingMode;
	}

	@Override
	public void enableTrainingMode(boolean shouldTrain) {
		this.isInTrainingMode = shouldTrain;
	}

	/**
	 * returns all possible actions for the pac as a list according to his
	 * current position. If the action would lead to a situation where the pac
	 * would move inside a wall this action is not possible and will
	 * not be included in the list.
	 * */
	@Override
	public List<Action> getCurrentPossibleActions() {
		List<Action> resultList = new ArrayList<>();

		int x = pac.getPosition().x;
		int y = pac.getPosition().y;

		if (maze.isWalkable(maze.getUpperNeighbor(x, y)))
			resultList.add(MoveAction.GO_NORTH);
		if (maze.isWalkable(maze.getLeftNeighbor(x, y)))
			resultList.add(MoveAction.GO_WEST);
		if (maze.isWalkable(maze.getLowerNeighbor(x, y)))
			resultList.add(MoveAction.GO_SOUTH);
		if (maze.isWalkable(maze.getRightNeighbor(x, y)))
			resultList.add(MoveAction.GO_EAST);

		return resultList;
	}

	public void endEpisode() {
		episodeEnded = true;

		if (isInLearningMode) {
			// if we are currently in training mode this means we
			// have to add 1 episode to the training level of the task.
			if (won || gameOver) {
				currentTask.addToTrainingLevel(1);
			}
		}
	}
}
