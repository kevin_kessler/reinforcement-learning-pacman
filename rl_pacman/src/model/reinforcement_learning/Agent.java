/**
 * 
 */
package model.reinforcement_learning;

import java.io.Serializable;
import java.util.List;

import model.reinforcement_learning.policy.Policy;
import model.reinforcement_learning.policy.ModelBasedPolicy;
//import model.reinforcement_learning.value_function.ValueFunction;
//import model.reinforcement_learning.value_function.ValueLearning;
import model.reinforcement_learning.value_function.ValueParams;

/**
 *	The agent represents the connection between reinforcement learning and
 *	the environment. He is responsible for determining the next action and
 *	updating his knowledge by using the policy.
 */
public class Agent implements Serializable {

	private static final long serialVersionUID = 4136538125201604231L;

	private Policy policy;

	protected Agent() {
		this(new ModelBasedPolicy());
	}
	
	protected Agent(Agent agtToCopy){
		this(agtToCopy.policy.getCopy());
	}

	protected Agent(Policy policy) {
		this.policy = policy;
	}

	public Policy getPolicy() {
		return policy;
	}

	public Action determineNextAction(State state, List<Action> possibleActions) {
		return policy.getNextAction(state, possibleActions);
	}

	/**
	 * Will update the state or state/action pair of the value function.
	 * Which params are necessary depends on the value function.
	 */
	public void rewardLastAction(ValueParams params) {
		// Feed value function (and maybe also the transition model)
		policy.updateValueFunction(params);
	}

}
