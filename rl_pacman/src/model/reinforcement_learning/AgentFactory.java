package model.reinforcement_learning;

import java.util.List;
import java.util.Map;

import model.environment.pacman.PacManState;
import model.reinforcement_learning.policy.ActionSelection;
import model.reinforcement_learning.policy.EpsGreedyActionSelection;
import model.reinforcement_learning.policy.GreedyActionSelection;
import model.reinforcement_learning.policy.ModelBasedPolicy;
import model.reinforcement_learning.policy.ModelFreePolicy;
import model.reinforcement_learning.policy.Policy;
import model.reinforcement_learning.policy.SoftMaxActionSelection;
import model.reinforcement_learning.value_function.ApproximateQLearning;
import model.reinforcement_learning.value_function.QLearning;
import model.reinforcement_learning.value_function.Sarsa;
import model.reinforcement_learning.value_function.Value;
import model.reinforcement_learning.value_function.ValueFunction;
import model.reinforcement_learning.value_function.ValueLearning;

/**
 * This class is responsible for creating correct agents. There are multiple
 * different configurations for the agent. Not every configuration needs a
 * transition model. This class provides functions which create an agent based
 * on the given parameters.
 *
 */
public class AgentFactory {

	public static Agent createAgentWith(ValueFunction valFunc, String actionSelectionName, double epsilon) {
		assert (valFunc != null && actionSelectionName != null);
		ActionSelection actSel = createActionSelectionFrom(actionSelectionName, epsilon);
		Policy policy = getPolicyFrom(valFunc, actSel);
		return new Agent(policy);
	}

	private static Policy getPolicyFrom(ValueFunction valFunc, ActionSelection actSel) {
		Policy policy;

		if (valFunc.getClass() == ApproximateQLearning.class) {
			policy = new ModelFreePolicy(actSel);
		} else if (valFunc instanceof QLearning) {
			policy = new ModelFreePolicy(actSel);
		} else if (valFunc.getClass() == ValueLearning.class) {
			policy = new ModelBasedPolicy(actSel);
		} else {
			throw new RuntimeException("AgentFactory getPolicyFrom: Unknown ValueFunction " + valFunc.getClass().getSimpleName());
		}
		
		policy.setValueFunction(valFunc);

		return policy;
	}

	public static Agent createAgentWith(ValueFunction valFunc, String actionSelectionString) {
		return createAPacManAgentWith(actionSelectionString, actionSelectionString, 0.3, 0.0, 0.0);
	}

	/**
	 * Default means an agent with model based policy, greedy action selection
	 * and Value-Iteration as value function.
	 * 
	 * @return
	 */
	public static Agent getDefaultAgent() {
		ActionSelection actSel = new GreedyActionSelection();
		ValueFunction valFunc = new ValueLearning();
		Policy policy = getPolicyFrom(valFunc, actSel);
		return new Agent(policy);
	}

	/**
	 * Default means an agent with model based policy, greedy action selection
	 * and ApproxQLearning as value function.
	 * 
	 * @return
	 */
	public static Agent getDefaultApproxQLearningAgent() {
		return createAPacManAgentWith(ValueFunction.APPROX_QLEARNING, ActionSelection.GREEDY, 0.3, 0.2, 0.9);
	}

	/**
	 * Creates an agent for the pac man environment (not simulation)
	 */
	public static Agent createAPacManAgentWith(String valFuncName, String actionSelectionName, double epsilon, double learningRate,
			double discountRate) {
		assert (valFuncName != null && actionSelectionName != null);


		ActionSelection actSel = createActionSelectionFrom(actionSelectionName, epsilon);

		ValueFunction valFunc;
		Policy policy;

		switch (valFuncName) {
		case ValueFunction.APPROX_QLEARNING:
			valFunc = new ApproximateQLearning(PacManState.NR_OF_FEATURES, learningRate, discountRate);
			policy = getPolicyFrom(valFunc, actSel);
			break;
		default:
			// TODO add more algorithms of new ones are implemented
			throw new RuntimeException("AgentFactory: Training Method " + valFuncName + " not supported");

		}

		return new Agent(policy);
	}

	public static Agent createAPacManAgentWith(String valFuncName, String actionSelectionName) {
		return createAPacManAgentWith(valFuncName, actionSelectionName, 0.3, 0.0, 0.0);
	}

	/**
	 * @param epsilon
	 *            a double value in the range 0..1. This is also the temp
	 *            variable for SoftMaxActionSelection.
	 */
	private static ActionSelection createActionSelectionFrom(String actionSelectionName, double epsilon) {
		ActionSelection actSel;
		switch (actionSelectionName) {
		case ActionSelection.GREEDY:
			actSel = new GreedyActionSelection();
			break;
		case ActionSelection.EPSGREEDY:
			actSel = new EpsGreedyActionSelection(epsilon);
			break;
		case ActionSelection.SOFTMAX:
			actSel = new SoftMaxActionSelection(epsilon);
			break;
		default:
			throw new RuntimeException("AgentFactory: Action Selection Policy " + actionSelectionName + " not supported");

		}
		return actSel;
	}

	/**
	 * Creates a new agent with a Value-Function based on valueFunctionName and a Action-Selection Method based on actionSelectionName.
	 * @param valueFunctionName
	 * @param actionSelectionName
	 * @param epsilon epsilon / temperature for Action-Selection 
	 * @param initActions
	 * @param discountRate Discount-Rate for Value-Function (Gamma)
	 * @param learningRate Learning-Rate for Value-Function (Alpha)
	 * @param terminalStates
	 * @param initialStateValues
	 * */
	public static Agent createAgentWith(String valueFunctionName, String actionSelectionName, double epsilon, List<Action> initActions,
			double discountRate, double learningRate, List<State> terminalStates, Map<Long, Map<Long, Value>> initialStateValues) {

		ValueFunction vf = createValueFunctionFrom(valueFunctionName, initActions, discountRate, learningRate, terminalStates, initialStateValues);
		Policy policy = getPolicyFrom(vf, createActionSelectionFrom(actionSelectionName, epsilon));
		return new Agent(policy);

	}

	/**
	 * Creates a value function based on valueFunctionName. Depending on the value function different parameters
	 * have to be provided.
	 * * VALUE_LEARNING: learningRate, discountRate
	 * * Q_LEARNING: initialStateValues, initActions, learningRate, discountRate
	 * * SARSA: initialStateValues, initActions, learningRate, discountRate, terminalStates
	 */
	private static ValueFunction createValueFunctionFrom(String valueFunctionName, List<Action> initActions, double discountRate,
			double learningRate, List<State> terminalStates, Map<Long, Map<Long, Value>> initialStateValues) {
		switch (valueFunctionName) {
		case ValueFunction.VALUE_LEARNING:
			return new ValueLearning(learningRate, discountRate);
		case ValueFunction.Q_LEARNING:
			return new QLearning(initialStateValues, initActions, learningRate, discountRate);
		case ValueFunction.SARSA:
			return new Sarsa(initialStateValues, initActions, learningRate, discountRate, terminalStates);
		default:
			throw new RuntimeException("Unknown q value function: " + valueFunctionName);
		}
	}

	/**
	 * updates the value function of the agent with a new discountRate and a new
	 * learningRate without resetting the current learning progress.
	 * */
	public static Agent updateAgentValueFunctionWith(Agent agent, double discountRate, double learningRate) {
		ValueFunction vf = agent.getPolicy().getValueFunction();

		if (vf instanceof QLearning) {
			((QLearning) vf).setLearningRate(learningRate);
			((QLearning) vf).setDiscountRate(discountRate);
		} else if (vf instanceof ValueLearning) {
			((ValueLearning) vf).setLearningRate(learningRate);
			((ValueLearning) vf).setDiscountRate(discountRate);
		} else if (vf instanceof ApproximateQLearning) {
			((ApproximateQLearning) vf).setLearningRate(learningRate);
			((ApproximateQLearning) vf).setDiscountRate(discountRate);
		}
		// TODO add more ValueFunction Types if necessary
		return agent;
	}

	/**
	 * Updates the Agent and sets a new action selection with a new epsilon /
	 * temperature value without resetting the current learning progress
	 * 
	 * @param epsilon
	 *            epsilon for epsilon greedy algorithms or temperature for
	 *            softmax
	 * */
	public static Agent updateAgentActionSelectionWith(Agent agent, String actionSelectionName, double epsilon) {
		agent.getPolicy().setActionSelectionMethod(createActionSelectionFrom(actionSelectionName, epsilon));
		return agent;
	}

	public static Agent copy(Agent agentToCopy) {
		return new Agent(agentToCopy);
	}

}
