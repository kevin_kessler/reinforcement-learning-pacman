package model.reinforcement_learning;


import java.util.List;

import model.environment.EnvironmentChangeListener;

/**
 * Contains all methods which are necessary for the agent
 */
public interface Environment {

	/*
	 * One episodes represents one game.<br/> This means either all dots in a
	 * maze are eaten or<br/> pac was caught by a ghost.
	 */
	public void performOneEpisode();
	
	public void cancelEpisode();

	/**
	 * One time step represents the transition from t to t+1. Invokes the
	 * RL-agent and executes received action.
	 */
	public void performOneTimeStep();

	/**
	 * Method enables training. This means the agent will receive rewards and
	 * updates his value function.
	 */
	public void enableLearning(boolean shouldTrain);
	/**
	 * True if training is enabled and the agent will learn and update his value
	 * function.
	 */
	public boolean isInLearningMode();
	
	public boolean isInTrainingMode();
	
	public void enableTrainingMode(boolean isInTrainingMode);
	
	public void addEnvironmentChangedListener(EnvironmentChangeListener listener);
	
	public void removeEnvironmentChangedListener(EnvironmentChangeListener listener);
	
	public List<Action> getCurrentPossibleActions();
}
