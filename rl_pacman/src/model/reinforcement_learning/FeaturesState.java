package model.reinforcement_learning;

import java.util.List;

import model.reinforcement_learning.value_function.ValueParams;

/**
 * An abstract state which describes the real state with features. If the state
 * space is too big a FeatureState can be used. See
 * https://www.youtube.com/watch?v=yNeSFbE1jdY 33:00
 */
public abstract class FeaturesState extends State {

	public abstract int getNumberOfFeatures();

	public abstract double[] getFeatures(ValueParams params);

	public abstract List<Action> getPossibleActions();

}
