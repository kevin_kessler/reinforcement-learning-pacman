/**
 * 
 */
package model.reinforcement_learning;

/**
 * An abstract interface which represents the natural numbers. The RL framework
 * does not need more than an unique number for a state.
 */
public abstract class State {

	/**
	 * The abstract identifier of this state. Should be unique for
	 * every state.
	 */
	public abstract long getSimpleId();

	// The following functions we need for storing the objects in hash maps.
	public abstract int hashCode();

	public abstract boolean equals(Object obj);
	
	public State(){
		
	}
	
	public State(State stateToCopy){
		
	}
	
	public abstract State getCopy();

}
