package model.reinforcement_learning;

public class StateActionPair {

	private String id;
	private State state;
	private Action action;
	
	public StateActionPair(State s, Action a){
		this.state = s;
		this.action = a;
		
		if(null == s || null == a)
			id = "";
		else
			id = a.getSimpleId() + "" + s.getSimpleId();
	}
	
	public StateActionPair(StateActionPair sapToCopy){
		this.state = sapToCopy.state.getCopy();
		this.action = sapToCopy.action.getCopy();
		this.id = sapToCopy.id;
	}

	public State getState() {
		return state;
	}


	public Action getAction() {
		return action;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StateActionPair other = (StateActionPair) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (action.getSimpleId() != other.action.getSimpleId())
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (state.getSimpleId() != other.state.getSimpleId())
			return false;
		return true;
	}

	
	
	
}
