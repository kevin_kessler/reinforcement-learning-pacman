package model.reinforcement_learning.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.value_function.Value;

/**
 * Superclass for the three action selection methods GREEDY, EPSGREEDY and
 * SOFTMAX, which ensures that all of them have the "selectAction" method
 * implemented.
 */
public abstract class ActionSelection implements Serializable {

	private static final long serialVersionUID = -2017263902550249225L;
	public final static String GREEDY = "Greedy";
	public final static String EPSGREEDY = "Epsilon-Greedy";
	public final static String SOFTMAX = "Softmax";
	public static final List<String> ACTION_SELECTION_METHODS = Collections.unmodifiableList(new ArrayList<String>(Arrays.asList(GREEDY, EPSGREEDY,
			SOFTMAX)));

	protected static final Random random = new Random();

	private String name;
	public ActionSelection(String name){
		this.name = name;
	}
	
	/**
	 * Chooses one action among the given ActionValuePairs. The way how or which
	 * action is chosen depends on the specific action selection sub class.
	 * 
	 * @param estimatedActionValuePairs
	 *            A list with ActionValuePairs among which the action will be
	 *            selected.
	 * @return The action that has been selected by the underlying
	 *         implementation.
	 */
	public abstract Action selectAction(List<ActionValuePair> estimatedActionValuePairs);

	public abstract ActionSelection getCopy();
	
	public String getName(){
		return name;
	}
}

/**
 * Helper class that helps managing the combination of an action and a value
 * that belong together
 */
class ActionValuePair {
	Action action;
	Value value;

	ActionValuePair(Action a, Value v) {
		action = a;
		value = v;
	}

	@Override
	public String toString() {
		return String.format("[%s; %s] ", action.toString(), value.toString());
	}
}
