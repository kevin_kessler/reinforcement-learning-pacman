package model.reinforcement_learning.policy;

import java.util.List;

import utils.MyMath;
import model.reinforcement_learning.Action;

/**
 * This ActionSelection method chooses always the 'best' action, meaning the
 * action that leads to the highest value among the given possible actions. If
 * there are several 'best' actions (lead to the same value), the action is
 * chosen randomly among those.
 */
public class GreedyActionSelection extends ActionSelection {

	private static final long serialVersionUID = -5209933336055670057L;

	public GreedyActionSelection() {
		super(GREEDY);
	}
	
	@Override
	public Action selectAction(List<ActionValuePair> estimatedActionValuePairs) {

		assert (null != estimatedActionValuePairs);

		// best action is not yet known
		ActionValuePair bestPair = estimatedActionValuePairs.get(random.nextInt(estimatedActionValuePairs.size()));

		// compare all actionvaluepairs with each other and choose the one with
		// the highest value
		for (ActionValuePair pair : estimatedActionValuePairs) {

			// choose randomly if both values are equal (prevents endless loops)
			if (MyMath.almostEquals(bestPair.value.getValue(), pair.value.getValue())) {
				boolean changeIt = random.nextBoolean();
				if (changeIt) {
					bestPair = pair;
				}
			}

			// choose action if its estimatedValue is better than the current
			// bestValue
			else if (bestPair.value.getValue() < pair.value.getValue()) {
				bestPair = pair;
			}
		}
		return bestPair.action;
	}

	@Override
	public ActionSelection getCopy() {
		return new GreedyActionSelection();
	}

}
