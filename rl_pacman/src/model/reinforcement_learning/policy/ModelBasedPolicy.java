package model.reinforcement_learning.policy;

import java.util.List;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.State;
import model.reinforcement_learning.value_function.Value;
import model.reinforcement_learning.value_function.ValueParams;

/**
 * This class is used for model based learning. This means we have to build the
 * transition model T of the MDP (Markov Decision Process) to determine which
 * state/action-pair leads to which successor state. This information is used
 * to calculate expected values within the getEstimatedValue method.
 * On calling updateValueFunction the transition model will automatically updated.
 */
public class ModelBasedPolicy extends Policy {

	private static final long serialVersionUID = -2176200110862452959L;

	/**
	 * This is the transition model T of the MDP which is used to determine
	 * which state/action-pair leads to which successor state. It is initialized
	 * with zero probabilities, meaning that at the beginning the probabilities are
	 * random. They will be updated during learning together with the value function.
	 * 
	 * TODO: currently ModelBasedPolicies are only used in simulation mode.
	 * thus, they will never be serialized. hence the keyword transient. this
	 * has to be changed, if ModelBasedPolicies need to be serialized in future.
	 * TansitionModel then needs to be serializable and should use stateIDs
	 * rather than stateobjects, since only the ids should be serialized
	 */
	transient protected TransitionModel transitionModel;

	/**
	 * Keeps track of the previously used possibile actions.
	 * This is used to update the transitionmodel whenever the 
	 * value function is updated, meaning after each taken action.
	 */
	private List<Action> lastPossibleActions;

	public ModelBasedPolicy(ActionSelection actionSelection) {
		super(actionSelection);
		transitionModel = new TransitionModel();
	}

	public ModelBasedPolicy() {
		this(new GreedyActionSelection());
	}

	public ModelBasedPolicy(ModelBasedPolicy mbpToCopy) {
		super(mbpToCopy);
		this.transitionModel = new TransitionModel(mbpToCopy.transitionModel);
		for (Action a : lastPossibleActions) {
			this.lastPossibleActions.add(a.getCopy());
		}
	}

	@Override
	public Policy getCopy() {
		return new ModelBasedPolicy(this);
	}

	@Override
	public void updateValueFunction(ValueParams params) {
		super.updateValueFunction(params);
		transitionModel.update(lastPossibleActions, params);
	}

	@Override
	public Action getNextAction(State state, List<Action> possibleActions) {
		// used to update the transition model
		lastPossibleActions = possibleActions;

		return super.getNextAction(state, possibleActions);
	}

	@Override
	public Value getEstimatedValue(State currentState, Action action) {
		// determine most probable successor state
		State successorState = transitionModel.getMostProbableSuccessorState(currentState, action);

		if (null == successorState)
			return new Value(-Double.MAX_VALUE);

		// return max value of the determined successorState
		return valueFunction.getMaxValueOf(successorState);
	}

}
