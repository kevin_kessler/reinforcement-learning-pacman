package model.reinforcement_learning.policy;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.State;
import model.reinforcement_learning.value_function.Value;
import model.reinforcement_learning.value_function.ValueParams;

/**
 * This class is used for model free learning. This means we do NOT build the
 * transition model T of the MDP (see ModelBasedPolicy).
 */
public class ModelFreePolicy extends Policy {

	private static final long serialVersionUID = -2176200110862452959L;

	public ModelFreePolicy(ActionSelection actionSelection) {
		super(actionSelection);
	}

	public ModelFreePolicy() {
		this(new GreedyActionSelection());
	}
	
	public ModelFreePolicy(ModelFreePolicy mfpToCopy){
		super(mfpToCopy);
	}
	
	@Override
	public Policy getCopy() {
		return new ModelFreePolicy(this);
	}

	@Override
	public void updateValueFunction(ValueParams params) {
		super.updateValueFunction(params);
	}

	@Override
	public Value getEstimatedValue(State currentState, Action action) {
		ValueParams vp = new ValueParams(currentState, action);
		return valueFunction.getValue(vp);
	}

}
