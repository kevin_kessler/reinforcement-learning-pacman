package model.reinforcement_learning.training;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import model.reinforcement_learning.Environment;

/**
 * This class trains a given task.
 *
 */
public class Training {

	protected final static Logger log = Logger.getLogger(Training.class.getName());

	private List<TrainingListener> trainingListener;

	private Environment env;

	private int nrOfEpisodes;
	private int currentEpisode;
	private boolean previousLearningMode;
	private boolean canceled = false;
	private int lastPercent = 0;	

	/**
	 * Responsible for training the agent from task in env.
	 * 
	 * @param task
	 * @param env
	 */
	public Training(Environment env) {	
		this.env = env;
	}

	public void train(int nrOfEpisodes) {
		canceled = false;
		lastPercent = 0;
		previousLearningMode = env.isInLearningMode();
		env.enableLearning(true);

		this.nrOfEpisodes = nrOfEpisodes;
		this.currentEpisode = 1;

		while (Training.this.currentEpisode <= Training.this.nrOfEpisodes && !canceled) {
			Training.this.env.performOneEpisode();
			Training.this.currentEpisode++;

			int percent = (int) (100.0 * currentEpisode / nrOfEpisodes);

			//inform ui about progress every 5%
			if ((percent % 5 == 0) && percent > lastPercent) {
				notifyListenerTrainingProgress(percent);
				lastPercent = percent;
			}
		}		

		notifyListenerTrainingFinished(canceled);
		Training.this.env.enableLearning(previousLearningMode);
	}

	/**
	 * Cancels a running training. A running episode will also be canceled.
	 */
	public void cancel() {
		canceled = true;
		Training.this.env.cancelEpisode();
	}

	private void notifyListenerTrainingFinished(boolean canceled) {

		if (trainingListener == null)
			return;

		for (TrainingListener l : trainingListener) {
			l.trainingFinished(canceled, currentEpisode);
		}
	}

	public void addTrainingListener(TrainingListener listener) {
		if (trainingListener == null)
			trainingListener = new ArrayList<TrainingListener>();

		trainingListener.add(listener);
	}

	private void notifyListenerTrainingProgress(int percent) {
		if (trainingListener == null)
			return;

		for (TrainingListener l : trainingListener) {
			l.reportTrainingProgress(percent);
		}
	}

	public void removeTrainingListener(TrainingListener listener) {
		trainingListener.remove(listener);
	}

	public Environment getEnv() {
		return env;
	}

}
