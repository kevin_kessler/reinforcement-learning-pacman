package model.reinforcement_learning.training;

/**
 * Used to notify about progress and end of a running training. 
 *
 */
public interface TrainingListener {
	
	public void trainingFinished(boolean canceled, int trainingEpisodes);

	public void reportTrainingProgress(int percent);

}
