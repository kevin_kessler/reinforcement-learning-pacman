package model.reinforcement_learning.value_function;

import java.util.List;
import java.util.Map;

import model.reinforcement_learning.Action;
import model.reinforcement_learning.Reward;
import model.reinforcement_learning.State;

public class Sarsa extends QLearning {

	private static final long serialVersionUID = 8302683292617338434L;
	
	private List<State> TERMINAL_STATES;

	/**
	 * @param actionsForInitStateMap
	 *            is used to initialize the map of a state if its map is null.
	 */
	public Sarsa(List<Action> actionsForInitStateMap, List<State> terminalStates) {
		this(null, actionsForInitStateMap, 0.1, 0.9, terminalStates);
	}

	/**
	 * 
	 * @param actionsForInitStateMap
	 *            is used to initialize the map of a state.
	 * @param learningRate
	 *            represents alpha of the update formula
	 * @param discountRate
	 *            represents gamma of the update formula
	 */
	public Sarsa(List<Action> actionsForInitStateMap, double learningRate, double discountRate, List<State> terminalStates) {
		this(null, actionsForInitStateMap, learningRate, discountRate, terminalStates);
	}

	/**
	 * 
	 * @param initialStateValues
	 *            can be used to initialize special states. For example if there
	 *            are states which have other actions than
	 *            actionsForInitStateMap then this map could provide already
	 *            maps for these states.
	 * @param actionsForInitStateMap
	 *            is used to initialize the map of a state.
	 * @param learningRate
	 *            represents alpha of the update formula
	 * @param discountRate
	 *            represents gamma of the update formula
	 */
	public Sarsa(Map<Long, Map<Long, Value>> initialStateValues, List<Action> actionsForInitStateMap, double learningRate,
			double discountRate, List<State> terminalStates) {
		super(initialStateValues, actionsForInitStateMap, learningRate, discountRate);
		name = SARSA;
		TERMINAL_STATES = terminalStates;
	}
	
	public Sarsa(Sarsa qviToCopy){
		super(qviToCopy);
		name = SARSA;
		previousState = qviToCopy.previousState;
		previousAction  = qviToCopy.previousAction;
		previousReward = qviToCopy.previousReward;
		previousSPrime = qviToCopy.previousSPrime;
	}
	
	@Override
	public ValueFunction getCopy() {
		return new Sarsa(this);
	}

	private State previousState;
	private Action previousAction;
	private Reward previousReward;
	private State previousSPrime;
	
	/**
	 * SARSA uses the quintuple (s,a,r,s',a') to update a value. Note the difference to QLearning is that it takes
	 * not always the max_a Q(s',a) but it takes the actual taken action a'.
	 * There are two things to care about since we only receive the tuple (s,a,r,s'), missing a'. 
	 * First we cannot update on the first call of this function since a' is missing. We have to wait for the 
	 * next call.
	 * Second we have to check if current s' is a terminal state. In this case we have to update the the current
	 * state s with (s,a,r,s'), without a', otherwise we would never update this s.
	 */
	@Override
	public void updateValueWith(ValueParams params) {
		State state = params.getState();
		Action action = params.getAction();
		Reward reward = params.getReward();
		State sPrime = params.getSPrime();
		boolean sPrimeIsTerminal = TERMINAL_STATES.contains(sPrime);
		
		assert (null != state && null != sPrime && null != reward);
		
		if (previousUpdateValuesAreAvailable()) {
			assert(previousSPrime.equals(state));
			
			ValueParams previousParams = new ValueParams(previousState, previousAction);
			Value oldValue = getValue(previousParams);
			Value valOfSPrime = getValue(new ValueParams(previousSPrime, action));
			double sample = previousReward.getValue() + discountRate * valOfSPrime.getValue();
			double newVal = oldValue.getValue() + learningRate * (sample - oldValue.getValue());

			insertNewValue(previousParams, newVal);
			
			if (sPrimeIsTerminal) {  // Then we need to update s otherwise this state will never be updated
				oldValue = getValue(params);
				valOfSPrime = getMaxValueOf(sPrime);
				sample = reward.getValue() + discountRate * valOfSPrime.getValue();
				newVal = oldValue.getValue() + learningRate * (sample - oldValue.getValue());

				insertNewValue(params, newVal);

				previousState = null;
				previousAction = null;
				previousReward = null;
				previousSPrime = null;
			}
		} // else we have to wait one update iteration
		
		if (!sPrimeIsTerminal){
			previousState = state;
			previousAction = action;
			previousReward = reward;
			previousSPrime = sPrime;
		}
	}

	private boolean previousUpdateValuesAreAvailable() {
		return  previousState != null
				&& previousAction != null
				&& previousReward != null
				&& previousSPrime != null;
	}
}
