package utils;

import javafx.application.Platform;
import model.Task;
import model.reinforcement_learning.Environment;

/**
 * This helperclass is used to perform a given number of episodes
 * in a seperate thread and provide a progress dialog at the same time.
 * The progressdialog can be accessed from outside class and can be used
 * to display the progress of the execution to the user.
 * 
 * The EpisodeHelper can be passed to a new Thread to be executed.
 * On thread.start() the EpisodeHelper's call() method will be invoked.
 */
public class EpisodeHelper extends javafx.concurrent.Task<Void> {

	/**
	 * The environment where the episodes should be performed in
	 */
	private final Environment env;
	
	/**
	 * the amount of episodes that shall be performed when running the task
	 */
	private final int nrOfEpisodes;
	
	/**
	 * the progressdialog which is used to show progress of the execution to the user
	 */
	private final ProgressBarDialog progressDialog;
	
	/**
	 * A task object which will be saved / serialized to file after the execution finished.
	 * This is needed to update / keep statistics of the given task according to the
	 * performed episodes.
	 */
	private final Task taskToSaveOnFinish;
	
	/**
	 * indicates whether the task is currently performing
	 */
	private boolean isHelping = false;
	
	/**
	 * Instantiates an EpisodeHelper object.
	 * @param env
	 * 			The environment where the episodes should be performed in
	 * @param nrOfEpisodes
	 * 			the amount of episodes that shall be performed when running the task
	 * @param taskToSaveOnFinish
	 * 			A task object which will be saved / serialized to file after the execution finished.
	 */
	public EpisodeHelper(Environment env, int nrOfEpisodes, Task taskToSaveOnFinish) {
		this.env = env;
		this.nrOfEpisodes = nrOfEpisodes;
		this.taskToSaveOnFinish = taskToSaveOnFinish;

		// Set up dialog
		progressDialog = new ProgressBarDialog();
		progressDialog.bindTo(this.progressProperty());
	}
	
	public Environment getEnvironment() {
		return env;
	}

	public ProgressBarDialog getProgressBarDialog() {
		return progressDialog;
	}

	/**
	 * Performs the specified number of episodes on the environment
	 * and updates the progress of the progress dialog after each episode.
	 * When all episodes are finished, the helper's attached task is being
	 * overwritten to save collected statistics. Afterwards the progressdialog
	 * is closed and the thread ends.
	 */
	@Override
	public Void call() {
		isHelping = true;
		
		int i=0;
		while(i<nrOfEpisodes && isHelping){
			env.performOneEpisode();
			updateProgress(++i, nrOfEpisodes);
		}
		
		taskToSaveOnFinish.overwrite();
		
		closeDialog();
		
		isHelping = false;	
		
		return null;
	}
	
	/**
	 * Cancels the execution of the episodehelper
	 */
	public void abort(){
		isHelping = false;
		env.cancelEpisode();
	}
	
	/**
	 * closes the progress dialog on the ui thread.
	 */
	private void closeDialog(){
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				progressDialog.close();
			}
		});
	}
}
