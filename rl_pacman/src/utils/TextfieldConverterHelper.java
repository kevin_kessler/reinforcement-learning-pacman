package utils;

import javafx.scene.control.TextField;

/**
 * Helper class for converting the input of a JavaFX Textfield to double,
 * integer, float or String. Is used in several controller classes for easy
 * handling of user input and creating meaningfull exception messages.
 * */
public class TextfieldConverterHelper {

	/**
	 * Ensures that the Textfield contains a valid positive double value in the
	 * range between 0.0 and 1.0. The double value will be returned for further
	 * usage
	 * 
	 * @param f
	 *            the Textfield to take the double from
	 * @param fieldname
	 *            should be a human readable name for the Textfield
	 * @throws Exception
	 *             if the value in the Textfield could not be converted to an
	 *             double value or the value is not in the proper range between
	 *             0.0 and 1.0. The message of the exception is human readable
	 *             and can be used for user information.
	 * */
	public static double getPositiveDoubleSmallerOne(TextField t, String fieldname) throws Exception {
		double d = 0.0;
		if ("".equals(t.getText().trim())) {
			throw new Exception(fieldname + " should not be empty.");
		}
		try {
			d = Double.parseDouble(t.getText().replace(",", ".").trim());
			if (d < 0.0 || d > 1.0) {
				throw new Exception(fieldname + " must be a number between 0.0 and 1.0.");
			}
		} catch (NumberFormatException e) {
			throw new Exception(fieldname + " is not a number.");
		}
		return d;
	}

	/**
	 * Ensures that the Textfield contains a valid positive double value greater
	 * zero. The double value will be returned for further usage
	 * 
	 * @param f
	 *            the Textfield to take the double from
	 * @param fieldname
	 *            should be a human readable name for the Textfield
	 * @throws Exception
	 *             if the value in the Textfield could not be converted to an
	 *             double value or the value is not greater zero. The message of
	 *             the exception is human readable and can be used for user
	 *             information.
	 * */
	public static double getPositiveDoubleGreaterZero(TextField t, String fieldname) throws Exception {
		double d = 0.0;
		if ("".equals(t.getText().trim())) {
			throw new Exception(fieldname + " should not be empty.");
		}
		try {
			d = Double.parseDouble(t.getText().replace(",", ".").trim());
			if (d < 0.0 || MyMath.almostEqualsToZero(d)) {
				throw new Exception(fieldname + " must be a number greater than zero");
			}
		} catch (NumberFormatException e) {
			throw new Exception(fieldname + " is not a number.");
		}
		return d;
	}

	/**
	 * Ensures that the Textfield contains a valid positive double value greater
	 * or equal to zero. The double value will be returned for further usage
	 * 
	 * @param f
	 *            the Textfield to take the double from
	 * @param fieldname
	 *            should be a human readable name for the Textfield
	 * @throws Exception
	 *             if the value in the Textfield could not be converted to an
	 *             double value or the value is not greater or equal to zero.
	 *             The message of the exception is human readable and can be
	 *             used for user information.
	 * */
	public static double getPositiveDouble(TextField t, String fieldname) throws Exception {
		double d = 0.0;
		if ("".equals(t.getText().trim())) {
			throw new Exception(fieldname + " should not be empty.");
		}
		try {
			d = Double.parseDouble(t.getText().replace(",", ".").trim());
			if (d < 0.0 && !MyMath.almostEqualsToZero(d)) {
				throw new Exception(fieldname + " must be a positive number greater or equal to zero");
			}
		} catch (NumberFormatException e) {
			throw new Exception(fieldname + " is not a number.");
		}
		return d;
	}

	/**
	 * Ensures that the Textfield contains a valid positive float value and
	 * returns this value for further usage
	 * 
	 * @param f
	 *            the Textfield to take the float from
	 * @param fieldname
	 *            should be a human readable name for the Textfield
	 * @throws Exception
	 *             if the value in the Textfield could not be converted to an
	 *             float value or the value is negative. The message of the
	 *             exception will be human readable and can be displayed to the
	 *             user.
	 * */
	public static float getPositiveFloat(TextField t, String fieldname) throws Exception {
		float number = 0;
		if ("".equals(t.getText().trim())) {
			throw new Exception(fieldname + " should not be empty.");
		}
		try {
			number = Float.parseFloat(t.getText().replace(",", ".").trim());
			if (number < 0.0) {
				throw new Exception(fieldname + " should be a positive value.");
			}
		} catch (NumberFormatException e) {
			throw new Exception(fieldname + " is not a number.");
		}
		return number;
	}

	/**
	 * Ensures that the textfield contains a valid integer value and returns
	 * this value for further usage
	 * 
	 * @param f
	 *            the textfield to take the integer value from
	 * @param fieldname
	 *            should be a human readable name for the textfield. is used for
	 *            throwing an usefull exception message.
	 * @throws Exception
	 *             if the value in the textfield could not be converted to an
	 *             integer value. the message of the exception can be used for
	 *             output to the user.
	 * 
	 * */
	public static int getPositiveInt(TextField t, String fieldname) throws Exception {
		int number = 0;
		if ("".equals(t.getText().trim())) {
			throw new Exception(fieldname + " should not be empty.");
		}
		try {
			number = Integer.parseInt(t.getText().trim());
			if (number < 0) {
				throw new Exception(fieldname + " should be a positive Integer.");
			}
		} catch (NumberFormatException e) {
			throw new Exception(fieldname + " is not an integer.");
		}
		return number;
	}

	/**
	 * Ensures that the textfield contains a valid float value and converts it
	 * to a negative value if necessary
	 * 
	 * @param f
	 *            the textfield to take the float from
	 * @param fieldname
	 *            should be a human readable name for the textfield
	 * @throws Exception
	 *             if the value in the textfield could not be converted to an
	 *             float value. The message of the exception can be displayed to
	 *             the user.
	 * */
	public static float getNegativeFloat(TextField t, String fieldname) throws Exception {
		float number = 0;
		if ("".equals(t.getText().trim())) {
			throw new Exception(fieldname + " should not be empty.");
		}
		try {
			number = Float.parseFloat(t.getText().replace(",", ".").trim());
		} catch (NumberFormatException e) {
			throw new Exception(fieldname + " is not a number.");
		}
		return -Math.abs(number);
	}

	/**
	 * Ensures that the textfield contains a not empty string and returns this
	 * string for further usage
	 * 
	 * @param f
	 *            the textfield to take the float from
	 * @param fieldname
	 *            should be a human readable name for the textfield
	 * @throws Exception
	 *             if the textfield is empty.The Exception message can be
	 *             displayed to the user.
	 * */
	public static String getNotEmptyString(TextField t, String fieldname) throws Exception {
		String s = t.getText().trim();

		if (s.equals(""))
			throw new Exception(fieldname + " should not be empty");

		return s;
	}

}
