/**
 * 
 */
package view;

import java.awt.Point;
import java.util.List;

import model.environment.pacman.Dot;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 * This class is responsible for the appearence of
 * {@link model.environment.pacman.Dot} and corresponding animations.
 */
public class DotUI extends Circle {

	/**
	 * The model object that should be represented by this ui element
	 */
	private Dot dot;

	/**
	 * The 'disappearing' animation of the dot ui element
	 */
	private FadeTransition fadeTransition;

	/**
	 * Instantiates a dot ui element.
	 * 
	 * @param dot
	 *            the dot object to represent by this ui element
	 * @param radius
	 *            the radius of the ui element
	 * @param color
	 *            the color of the ui element
	 */
	public DotUI(Dot dot, double radius, Color color) {
		super(radius, color);
		assert (dot != null);
		this.dot = dot;
		this.setFill(color);
	}

	/**
	 * Instantiates a dot ui element.
	 * 
	 * @param dot
	 *            the dot object to represent by this ui element
	 * @param radius
	 *            the radius of the ui element
	 * @param color
	 *            the color of the ui element
	 * @param disappearDelay
	 *            the delay which the disappear animation as to wait until it is
	 *            played after invocation
	 */
	public DotUI(Dot dot, double radius, Color color, long disappearDelay) {
		this(dot, radius, color);

		this.fadeTransition = new FadeTransition(Duration.millis(1), this);
		fadeTransition.setToValue(0);
		setDisappearDelay(disappearDelay);
	}

	/**
	 * Instantiates a dot ui element.
	 * 
	 * @param dot
	 *            the dot object to represent by this ui element
	 * @param radius
	 *            the radius of the ui element
	 * @param color
	 *            the color of the ui element
	 * @param disappearDelay
	 *            the delay which the disappear animation as to wait until it is
	 *            played after invocation
	 * @param listToRemoveFrom
	 *            the UI-Element list which this ui element is part of. after
	 *            disappearing finished this ui element will be removed from the
	 *            list
	 */
	public DotUI(Dot dot, double radius, Color color, long disappearDelay, List<Node> listToRemoveFrom) {
		this(dot, radius, color, disappearDelay);

		this.fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				if (actionEvent != null && actionEvent.getSource() instanceof FadeTransition) {
					listToRemoveFrom.remove(this);
				}
			}
		});
	}

	public Dot getDot() {
		return dot;
	}

	public Point getGridPosition() {
		return dot.getPosition();
	}

	/**
	 * Starts the disappear animation of this ui element.
	 * Note: the animation has a preceding delay which passes before the actual animation starts.
	 * You can set the delay to zero to instantly start the animation when calling this method.
	 */
	public void disappear() {
		if (null != fadeTransition) {
			fadeTransition.playFromStart();
		}
	}

	public void setDisappearDelay(long delayDuration) {
		if (null != fadeTransition) {
			fadeTransition.setDelay(Duration.millis(delayDuration));
		}
	}

}
