package view;

import java.awt.Point;

import model.environment.MoveAction;
import model.environment.pacman.Ghost;
import model.reinforcement_learning.Action;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import java.io.File;

/**
 * Superclass for the specific ghost ui elements.
 * This class is responsible for the appearence and animation of
 * {@link model.environment.pacman.Ghost} and its subclasses.
 */
public abstract class GhostUI extends Rectangle implements MovableUI {

	/**
	 * Relative path to the directory where the ghost images are stored
	 */
	//public final static String GHOST_IMAGE_DIRECTORY_PATH = File.separator +"assets"+ File.separator + "images" + File.separator;
	public static final String GHOST_IMAGE_DIRECTORY_PATH = "file:///"+System.getProperty("user.dir") + File.separator + "ghosts" + File.separator;
	
	/**
	 * Whether the teleport animation is currently playing
	 */
	private boolean isTeleporting = false;
	
	/**
	 * Whether the blink animation is currently playing
	 */
	private boolean blinkAnimationIsRunning;
	
	/**
	 * The move animation which smoothly moves the ghost from one point to another
	 */
	private TranslateTransition moveTransition;
	
	/**
	 * The teleport 'animation' which instantly moves the ghost from one point to another
	 */
	private TranslateTransition teleportTransition;
	
	/**
	 * The blink animation which lets the ghost blink when the pac ate the last dot
	 */
	private Timeline blinkAnimation;

	/**
	 * The image that shows the ghost watching and walking to the left
	 */
	protected ImagePattern ghostLeft = null;
	
	/**
	 * The image that shows the ghost watching and walking to the right
	 */
	protected ImagePattern ghostRight = null;
	
	/**
	 * The image that shows the ghost watching and walking to the top
	 */
	protected ImagePattern ghostUp = null;
	
	/**
	 * The image that shows the ghost watching and walking to the bottom
	 */
	protected ImagePattern ghostDown = null;

	/**
	 * The model object which is being visualized by the ghos ui
	 */
	protected Ghost ghost;
	
	/**
	 * Instantiates a ghost ui object.
	 * @param blockSize
	 * 			the width and height of the UI Element
	 * @param moveDuration
	 * 			the duration of the move animation in ms
	 * @param leftImageName
	 * 			the filename of the left-image (inclusive file extension, without path)
	 * @param rightImageName
	 * 			the filename of the right-image (inclusive file extension, without path)
	 * @param upImageName
	 * 			the filename of the up-image (inclusive file extension, without path)
	 * @param downImageName
	 * 			the filename of the down-image (inclusive file extension, without path)
	 */
	protected GhostUI(double blockSize, long moveDuration, String leftImageName, String rightImageName, String upImageName, String downImageName) {
		super(blockSize, blockSize);
		setImagePatterns(leftImageName, rightImageName, upImageName, downImageName);
		
		this.blinkAnimation = initBlinkAnimation();
		this.moveTransition = new TranslateTransition(Duration.millis(moveDuration), this);
		this.teleportTransition = new TranslateTransition(Duration.millis(1), this);
		this.teleportTransition.setOnFinished(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				if (actionEvent != null && actionEvent.getSource() instanceof TranslateTransition) {
					isTeleporting = false;
				}
			}
		});
	}

	/**
	 * Creates ImagePattern objects for the different directions a ghost can turn to and assigns them to
	 * the corresponding variables.
	 */
	private void setImagePatterns(String leftImageName, String rightImageName, String upImageName, String downImageName) {
		ghostLeft = new ImagePattern(new Image(GHOST_IMAGE_DIRECTORY_PATH+leftImageName));
		ghostRight = new ImagePattern(new Image(GHOST_IMAGE_DIRECTORY_PATH+rightImageName));
		ghostUp = new ImagePattern(new Image(GHOST_IMAGE_DIRECTORY_PATH+upImageName));
		ghostDown = new ImagePattern(new Image(GHOST_IMAGE_DIRECTORY_PATH+downImageName));
	}

	/**
	 * initializes the blink animation
	 */
	private Timeline initBlinkAnimation() {
		Timeline reset = new Timeline(new KeyFrame(Duration.millis(200), new KeyValue(this.opacityProperty(), 1)), new KeyFrame(Duration.millis(400),
				new KeyValue(this.opacityProperty(), 0)), new KeyFrame(Duration.millis(600), new KeyValue(this.opacityProperty(), 1)));
		reset.setCycleCount(3);

		return reset;
	}

	/**
	 * Provides the corresponding Ghost Object for the UI Element
	 * */
	public Ghost getGhost() {
		return ghost;
	}

	/**
	 * Provides the Position of the corresponding Ghost Object for the UI
	 * Element
	 * */
	@Override
	public Point getGridPosition() {
		return ghost.getPosition();
	}

	
	@Override
	public void turnTo(Action action) {
		ImagePattern currentPattern = ghostDown;

		MoveAction gAction = (MoveAction) action;
		switch (gAction) {
		case GO_WEST:
			currentPattern = ghostLeft;
			break;
		case GO_EAST:
			currentPattern = ghostRight;
			break;
		case GO_NORTH:
			currentPattern = ghostUp;
			break;
		default:
			break;
		}
		super.setFill(currentPattern);
	}

	@Override
	public void moveTo(double screenX, double screenY) {
		moveTransition.stop();

		if (isTeleporting)
			return;

		moveTransition.setToX(screenX);
		moveTransition.setToY(screenY);
		moveTransition.play();
	}

	@Override
	public void teleportTo(double screenX, double screenY) {
		isTeleporting = true;

		moveTransition.stop();
		teleportTransition.stop();
		teleportTransition.setToX(screenX);
		teleportTransition.setToY(screenY);
		teleportTransition.play();

	}

	@Override
	public void playBlinkAnimation() {
		blinkAnimationIsRunning = true;
		blinkAnimation.playFromStart();
	}

	@Override
	public void stopBlinkAnimation() {
		blinkAnimation.stop();
		this.setOpacity(1);
		blinkAnimationIsRunning = false;
	}

	@Override
	public Timeline getBlinkAnimation() {
		return blinkAnimation;
	}

	@Override
	public boolean blinkAnimationIsRunning() {
		return blinkAnimationIsRunning;
	}
}
