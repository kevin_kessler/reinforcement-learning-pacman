/**
 * 
 */
package view;

import model.environment.pacman.Inky;

/**
 *
 * This class is responsible for the appearence of
 * {@link model.environment.pacman.Inky} and corresponding animations.
 */
public class InkyUI extends GhostUI {

	public InkyUI(Inky g, double blockSize, long moveDuration) {
		super(blockSize, moveDuration, "inkyl.png", "inkyr.png", "inkyu.png", "inkyd.png");

		assert (g != null);

		ghost = g;

		turnTo(this.getGhost().getLastAction());
	}

}
