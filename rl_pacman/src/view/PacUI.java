/**
 * 
 */
package view;

import java.awt.Point;
import java.util.logging.Level;
import java.util.logging.Logger;

import model.environment.MoveAction;
import model.environment.pacman.Pac;
import model.reinforcement_learning.Action;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.util.Duration;

/**
 * This class is responsible for the appearence of
 * {@link model.environment.pacman.Pac} and corresponding animations.
 */
public class PacUI extends Arc implements MovableUI {
	/**
	 * The origin of the mouth angle when mouth is opened in degree.
	 */
	private static final double MAX_MOUTH_POS = 70;
	
	/**
	 * The maximum angle of the opened mouth
	 */
	private static final double MAX_MOUTH_SIZE = 140;

	/**
	 * Whether the teleport animation is currently playing
	 */
	private boolean isTeleporting = false;
	
	/**
	 * Whether the blink animation is currently playing
	 */
	private boolean blinkAnimationIsRunning = false;
	
	/**
	 * Whether the game over animation is currently playing
	 */
	private boolean gameOverAnimationIsRunning = false;
	
	/**
	 * The move animation which smoothly moves the pac from one point to another
	 */
	private TranslateTransition moveTransition;
	
	/**
	 * The teleport 'animation' which instantly moves the pac from one point to another
	 */
	private TranslateTransition teleportTransition;
	
	/**
	 * The eat animation (mouth open, mouth closed) 
	 */
	private Timeline eatAnimation;
	
	/**
	 * The blink animation which lets the ghost blink when the pac ate the last dot
	 */
	private Timeline blinkAnimation;
	
	/**
	 * The animation which is played when pac goes game over (lost all his lives)
	 */
	private RotateTransition gameOverAnimation;
	
	/**
	 * The model object which is represented by this ui element
	 */
	private Pac pac;
	
	/**
	 * the radius of this ui element
	 */
	private double radiusXY;
	
	/**
	 * the color of this ui element
	 */
	private Color color;

	/**
	 * Instantiates a pac ui element.
	 * @param pac
	 * 			The model object which should be represented by this ui element
	 * @param radius
	 * 			the intended radius of the ui element
	 * @param color
	 * 			the intended color of the ui element
	 * @param moveDuration
	 * 			the duration of the move animation in ms
	 */
	public PacUI(Pac pac, double radius, Color color, long moveDuration) {
		super(radius, radius, radius, radius, 0, 360);
		assert (pac != null);
		this.pac = pac;
		this.setType(ArcType.ROUND);
		this.setFill(color);
		this.color = color;
		this.eatAnimation = initEatAnimation();
		this.blinkAnimation = initBlinkAnimation();
		this.radiusXY = radius;
		this.moveTransition = new TranslateTransition(Duration.millis(moveDuration), this);
		this.teleportTransition = new TranslateTransition(Duration.millis(1), this);
		
		this.teleportTransition.setOnFinished(new EventHandler<ActionEvent>() {
	        @Override 
	        public void handle(ActionEvent actionEvent) {
	            if(actionEvent != null && actionEvent.getSource() instanceof TranslateTransition){
	            	isTeleporting = false;
	            }
	        }
	    });
		
		this.gameOverAnimation = new RotateTransition(Duration.millis(400), this);
		this.gameOverAnimation.setFromAngle(0);
		this.gameOverAnimation.setToAngle(360);
		this.gameOverAnimation.setCycleCount(3);
	}

	public Pac getPac() {
		return this.pac;
	}

	public double getRadiusXY() {
		return this.radiusXY;
	}

	/**
	 * starts the eating animation. does not stop until stopEatAnimation() is called
	 */
	public void playEatAnimation() {
		this.eatAnimation.play();
	}

	/**
	 * stops the eating animation
	 */
	public void stopEatAnimation() {
		this.eatAnimation.stop();
	}

	/**
	 * setup of the eat animation
	 * @return
	 * 		a timeline representing the eat animation
	 */
	private Timeline initEatAnimation() {
		Timeline anim = new Timeline(
				new KeyFrame(Duration.seconds(0.25), 
						new KeyValue(this.startAngleProperty(), MAX_MOUTH_POS), 
						new KeyValue(this.lengthProperty(), 360 - MAX_MOUTH_SIZE)), 
				new KeyFrame(Duration.seconds(0.25), 
						new KeyValue(this.startAngleProperty(), 0),
						new KeyValue(this.lengthProperty(), 360))
				);
		
		anim.setAutoReverse(true);
		anim.setCycleCount(Timeline.INDEFINITE);
		return anim;
	}
	
	/**
	 * setup of the blink animation.
	 * @return
	 * 		a timeline representing the blink animation
	 */
	private Timeline initBlinkAnimation() {
		Timeline reset = new Timeline(
				new KeyFrame(
                        Duration.millis(200),
                        new KeyValue(this.opacityProperty(), 1),
                        new KeyValue(this.fillProperty(), color)
                ),
                new KeyFrame(
                		Duration.millis(400),
                        new KeyValue(this.opacityProperty(), 0),
                        new KeyValue(this.fillProperty(), Color.RED)
                ),
                new KeyFrame(
                		Duration.millis(600),
                        new KeyValue(this.opacityProperty(), 1),
                        new KeyValue(this.fillProperty(), color)
                )
        );
		reset.setCycleCount(3);
		
        return reset;
	}

	@Override
	public double getWidth() {
		return this.radiusXY * 2;
	}

	@Override
	public double getHeight() {
		return getWidth();
	}

	@Override
	public Point getGridPosition() {
		return pac.getPosition();
	}

	@Override
	public void turnTo(Action action) {
		MoveAction pAction = (MoveAction) action;
		switch (pAction) {
		case GO_NORTH:
			this.setRotate(270);
			break;
		case GO_EAST:
			this.setRotate(0);
			break;
		case GO_SOUTH:
			this.setRotate(90);
			break;
		case GO_WEST:
			this.setRotate(180);
			break;
		default: // no change to ui if action was DO_NOTHING
			break;
		}
	}

	@Override
	public void moveTo(double screenX, double screenY) {
		Logger.getGlobal().log(Level.FINER, "Pac moves to: "+screenX + " " + screenY);
		
		moveTransition.stop();
		
		if(isTeleporting)
			return;
		
		moveTransition.setToX(screenX);
		moveTransition.setToY(screenY);
		moveTransition.play();
	}

	@Override
	public void teleportTo(double screenX, double screenY) {
		Logger.getGlobal().log(Level.FINER, "Pac teleports to: "+screenX + " " + screenY);
		isTeleporting = true;
		moveTransition.stop();
		teleportTransition.stop();
		teleportTransition.setToX(screenX);
		teleportTransition.setToY(screenY);
		teleportTransition.play();
	}
	
	@Override
	public void playBlinkAnimation(){
		blinkAnimationIsRunning = true;
		blinkAnimation.playFromStart();
	}
	
	public void playGameOverAnimation(){
		gameOverAnimationIsRunning = true;
		gameOverAnimation.playFromStart();
	}
	
	@Override
	public void stopBlinkAnimation(){
		blinkAnimation.stop();
		this.setFill(color);
		this.setOpacity(1);
		blinkAnimationIsRunning = false;
	}	
	
	/**
	 * stops the game over animation
	 */
	public void stopGameOverAnimation(){
		gameOverAnimation.stop();
		gameOverAnimationIsRunning = false;
	}
	
	@Override
	public Timeline getBlinkAnimation(){
		return blinkAnimation;
	}
	
	public RotateTransition getGameOverAnimation(){
		return gameOverAnimation;
	}
	
	/**
	 * determines whether the game over animation is running
	 */
	public boolean gameOverAnimationIsRunning(){
		return gameOverAnimationIsRunning;
	}
	
	@Override
	public boolean blinkAnimationIsRunning(){
		return blinkAnimationIsRunning;
	}
}
