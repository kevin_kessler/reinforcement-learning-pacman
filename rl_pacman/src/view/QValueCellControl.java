package view;

import java.io.IOException;
import java.text.DecimalFormat;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class QValueCellControl extends GridPane {

	@FXML
	private Label lblLeft;
	@FXML
	private Label lblRight;
	@FXML
	private Label lblUp;
	@FXML
	private Label lblDown;

	// the triangles for the color of the values
	@FXML
	private Polygon polyLeft;
	@FXML
	private Polygon polyRight;
	@FXML
	private Polygon polyUp;
	@FXML
	private Polygon polyDown;

	/*
	private final int ROTATION_FOR_LEFT = 90;
	private final int ROTATION_FOR_RIGHT = -90;
	private final int ROTATION_FOR_UP = 180;
	private final int ROTATION_FOR_DOWN = 0;*/

	private DecimalFormat formatter = new DecimalFormat("0.00");

	public QValueCellControl() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("QValueCell.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		
		polyLeft.setFill(Color.BLACK);
		polyRight.setFill(Color.BLACK);
		polyUp.setFill(Color.BLACK);
		polyDown.setFill(Color.BLACK);
		 
		polyLeft.setStroke(Color.DARKBLUE);
		polyRight.setStroke(Color.DARKBLUE);
		polyUp.setStroke(Color.DARKBLUE);
		polyDown.setStroke(Color.DARKBLUE);
		
	}
	

	public QValueCellControl(double left, double right, double up, double down) {
		this();
		this.setValueLeft(left);
		this.setValueRight(right);
		this.setValueUp(up);
		this.setValueDown(down);
	}

	public String getValueLeft() {
		return this.lblLeft.getText();
	}

	public String getValueRight() {
		return this.lblRight.getText();
	}

	public String getValueUp() {
		return this.lblUp.getText();
	}

	public String getValueDown() {
		return this.lblDown.getText();
	}

	public void setValueLeft(double value) {
		String sValue = formatter.format(value);
		polyLeft.setFill(getColorByValue(value));

		if (Math.abs(value) > 0.5) {
			lblLeft.setStyle("-fx-text-fill: black");
		} else {
			lblLeft.setStyle("-fx-text-fill: white");
		}

		this.lblLeft.setText(sValue);
	}

	public void setValueRight(double value) {

		String sValue = formatter.format(value);
		polyRight.setFill(getColorByValue(value));

		if (Math.abs(value) > 0.5) {
			lblRight.setStyle("-fx-text-fill: black");
		} else {
			lblRight.setStyle("-fx-text-fill: white");
		}

		this.lblRight.setText(sValue);
	}

	public void setValueUp(double value) {
		
		String sValue = formatter.format(value);
		polyUp.setFill(getColorByValue(value));

		if (Math.abs(value) > 0.5) {
			lblUp.setStyle("-fx-text-fill: black");
		} else {
			lblUp.setStyle("-fx-text-fill: white");
		}
		
		this.lblUp.setText(sValue);
	}

	public void setValueDown(double value) {
		String sValue = formatter.format(value);	
		polyDown.setFill(getColorByValue(value));

		if (Math.abs(value) > 0.5) {
			lblDown.setStyle("-fx-text-fill: black");
		} else {
			lblDown.setStyle("-fx-text-fill: white");
		}
		
		this.lblDown.setText(sValue);
	}
	

	/*
	 * creates a color object depending of a given double value. if the double
	 * value is negative the color will be red. otherwise the color will be
	 * green. opacity is set to value.
	 * 
	 * @param value should be a double value below or equal 1.0
	 */
	private Color getColorByValue(double value) {

		double b = 0.0;
		double r = (value > 0) ? 0.0 : 1.0;
		double g = (value > 0) ? 1.0 : 0.0;

		double opacity = Math.abs(value);
		if(opacity > 1.0) opacity = 1.0;

		return Color.color(r, g, b, opacity);
	}

}
