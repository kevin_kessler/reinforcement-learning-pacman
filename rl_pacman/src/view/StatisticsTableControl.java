package view;

import java.io.IOException;

import model.Statistics;
import model.Task;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class StatisticsTableControl extends VBox {

	@FXML
	private Label lblMazeName, lblActionSelection, lblTrainingMethod, lblTaskName;

	@FXML
	private CheckBox cbClyde, cbBlinky, cbPinky, cbInky;
	
	@FXML 
	private TableView<StatisticsUIModel> tblStatistics;

	private ObservableList<StatisticsUIModel> listData;

	@FXML
	private TableColumn<StatisticsUIModel, Integer> colGames, colWon, colTrainedEpisodes;
	@FXML
	private TableColumn<StatisticsUIModel, Double> colMinScore, colMaxScore, colAvgScore;

	public StatisticsTableControl(Task task) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("StatisticsTable.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
		
		assert (task != null);

		lblTaskName.setText(task.getName());
		lblMazeName.setText(task.getMazeName());

		if (task.getAgent() != null && task.getAgent().getPolicy() != null)
			lblActionSelection.setText(task.getAgent().getPolicy().getActionSelectionMethod().getName());

		if (task.getAgent() != null && task.getAgent().getPolicy().getValueFunction() != null)
			lblTrainingMethod.setText(task.getAgent().getPolicy().getValueFunction().getName());

		cbClyde.setSelected(task.isClydeSelected());
		cbBlinky.setSelected(task.isBlinkySelected());
		cbPinky.setSelected(task.isPinkySelected());
		cbInky.setSelected(task.isInkySelected());

		// FILL TABLE WITH STATISTIC DATA

		listData = FXCollections.observableArrayList();

		for (Statistics s : task.getStatisticsPerTrainingLevel()) {
			listData.add(new StatisticsUIModel(s));
		}

		// pecifies a cell factory for each column. The cell factories are
		// implemented by using the PropertyValueFactory class, which uses the
		// properties of the table columns as references to the corresponding
		// methods of the Person class.

		colTrainedEpisodes.setCellValueFactory(new PropertyValueFactory<StatisticsUIModel, Integer>("trainedEpisodes"));
		colGames.setCellValueFactory(new PropertyValueFactory<StatisticsUIModel, Integer>("nrGames"));
		colWon.setCellValueFactory(new PropertyValueFactory<StatisticsUIModel, Integer>("won"));
		colMinScore.setCellValueFactory(new PropertyValueFactory<StatisticsUIModel, Double>("min"));
		colMaxScore.setCellValueFactory(new PropertyValueFactory<StatisticsUIModel, Double>("max"));
		colAvgScore.setCellValueFactory(new PropertyValueFactory<StatisticsUIModel, Double>("avg"));

		tblStatistics.setItems(listData);
	}

}
